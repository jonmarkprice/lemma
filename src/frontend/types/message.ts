export enum MessageKind {
    Error   = 'ERROR',
    Victory = 'VICTORY',
    None    = 'NONE',
    // PREVIEW
}

export type Message = ErrorMessage | VictoryMessage | NoMessage;

export type NoMessage = {
    kind: MessageKind.None,
}

export type ErrorMessage = {
    kind: MessageKind.Error,
    text: string,
}

export type VictoryMessage = {
    kind: MessageKind.Victory,
    text: string,
}
