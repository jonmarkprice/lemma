import { StickID, StickState, XboxButton } from "./gamepad";
import { Timestamp } from "../types/gamepad";

// DEPRECATE
export const ADD_ACTION = 'ADD_ACTION';
// DEPRECATE
export const DISPLAY_STICK_INFO = 'DISPLAY_STICK_INFO';

// Currently, the MOVE_CURSOR is used in the Game,
// while MOVE_LEFT_STICK is used in Menu. The difference
// is that one is continuous and the other is discrete.

// For MOVE_CURSOR, we continually compute the angle between
// the currently selected node and the cursor. We can subdivide
// this angle by the number of children, in order to get ranges.
// If there are many, many children -- as in a large (associative)
// sum, this may become impractical.
// A natural way to do it might be to consider the distance from the
// line (or even just from the target point), and choose the closest
// to project onto, but then within a certain range, we would make
// a discrete move. This determination would happen in the reducer.

// The other way to do it would be to reserve certain sectors of the 
// circle (for siblings, parent, children), then subdivide the child
// sector by the number of children.... and then ... 
// no this doesn't work unless the tree is drawn in a way such that
// the angles are always the same.
// We just need to ensure that the angle is reasonable.
// Do we need to check that there is no overlap?
// I suppose we could check for membership in the range: 
// (theta - epsilon, theta + epsilon), for each angle.
// Then if there are multiple angles in the range, pick the closest.
// Math.min(children.map(angle => Math.abs(theta - angle))
// Here theta is the angle from the selected node to the cursor,
// epislon is the maximum offset that counts as "within".

// If the distance between the cursor and a child node becomes small
// enough, then we "fire" a discrete event.
// Alternatively, if the instanteous magnitude exceeds a certain threshold,
// then the projected node is automatically seleceted.
// After one of these discrete moves, the (x, y) coordinates of the cursor
// are set to exactly the (newly) selected node.
// How do we get the instanteous magnitude? I suppose it is computable
// from MOVE_LEFT_STICK.... wait no -- that's only the angle.

// We could modify MOVE_LEFT_STICK to do so.
// Or we could include the instanteous magnitude (and angle?)
// with MOVE_CURSOR.


export const MOVE_CURSOR = 'MOVE_CURSOR';
export const MOVE_LEFT_STICK = 'MOVE_LEFT_STICK';

export const MOVE_RIGHT_STICK = 'MOVE_RIGHT_STICK';
export const PRESS_BUTTON = 'PRESS_BUTTON';
export const CONNECT_CONTROLLER = 'CONNECT_CONTROLLER';


// DEPRECATED
export interface AddAction {
    type: typeof ADD_ACTION,
    value: string,
};

export interface MoveCursor {
    type: typeof MOVE_CURSOR,
    x: number,
    y: number,
    timestamp: Timestamp,
}

export interface MoveLeftStick {
    type: typeof MOVE_LEFT_STICK,
    degree: number,
}

export interface MoveRightStick {
    type: typeof MOVE_RIGHT_STICK,
    degree: number,
}

export interface PressButton {
    type: typeof PRESS_BUTTON,
    label: XboxButton,
}

export interface ConnectControllerAction {
    type: typeof CONNECT_CONTROLLER,
    gamepad: Gamepad,
};

export interface DisplayStickInfo {
    type: typeof DISPLAY_STICK_INFO,
    stick: StickID,
    state: StickState,
}
