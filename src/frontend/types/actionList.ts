export const ADD_ACTION = 'ADD_ACTION';

export interface AddAction {
    type: typeof ADD_ACTION,
    value: string,
};