export type Timestamp = number;

export type StickTiming = {
    x: number, // last value of x
    y: number, // last value of y
    timestamp: number, // last event
}

export type ButtonState = {
    wasPressed: boolean,
    firstFired: number,
    lastFired: number,
}

// Consider adding 'fireCount' (number) for hold/refire logic.
export type StickState = {
    lastMoved: Timestamp,
    lastX: number,
    lastY: number,
    wasMoved: boolean,
    isHeld: boolean,
    lastHeld: Timestamp,
}

export type GamepadState = {
    buttons: Record<XboxButton, ButtonState>,
    sticks: Record<StickID, StickState>,
};

export enum ButtonAction {
    Fire, // Dispatch press and set lastEvent to (now, true)
    Release,
        // reset lastEvent.time (to now) so that it fires immediately if pressed
    Held,
    NoOp,
};

export type XboxButton
    = 'A' | 'B' | 'X' | 'Y'
    | 'LB'| 'RB'
    | 'LT' | 'RT'
    | 'LM' | 'RM' // menu? (start, select?)
    | 'LSD' | 'RSD'
    | 'up' | 'down' | 'left' | 'right';

export enum StickAction {
    Move,
    StartHold,
    ContinueHold,
    Release,
    NoOp,
}

export enum StickID {
    Left = 'left',
    Right = 'right'
}