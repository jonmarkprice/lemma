import { Direction } from "../../core/display/focus";
import { Timestamp } from "./gamepad";

export type IAA = ApplyRule | FlipRule 
    | RuleSelection |  MenuSelect | ParentMenu | NextLevel
    | MenuVisit | MoveCursor | NoOp; // |TreeAction

// export interface TreeAction {
//     type: 'Game/tree',
//     direction: Direction,
// }

// // Is this different from tree action?
// // yes and no. It could be if we let direction be nullable.
export interface MoveCursor {
    type: 'Game/cursor',
    x: number,
    y: number,
    timestamp: Timestamp,
}

// export interface TreeAction {
//     type: 'Game/tree',
//     direction: Direction,
// }

export interface ApplyRule {
    type: 'Game/rule/apply',
}

export interface FlipRule {
    type: 'Game/rule/flip',
}

export interface RuleSelection {
    type: 'Game/rule/selection',
    index: number,
}

export interface MenuSelect {
    type: 'Menu/select',
    index: number,
}

export interface MenuVisit {
    type: 'Menu/visit',
    index: number,
}

export interface ParentMenu {
    type: 'Nav/back',
}

export interface NextLevel {
    type: 'Nav/next',
}

export interface NoOp {
    type: 'NoOp',
}
