// thinking of moving this to frontend/types...

import { Expression } from '../../domains/algebra/expression';
import { RuleObj } from '../../domains/algebra/rules';

export type Menu = MenuScreen | LevelScreen | ReadingScreen;

export enum ScreenKind {
    Menu = 'MENU',
    Level = 'LEVEL',
    Reading = 'READING',
};

export type MenuScreen = {
    title: string,
    kind: ScreenKind.Menu,
    menu: Menu[],
}

export type LevelScreen = {
    title: string,
    kind: ScreenKind.Level,
    prompt: JSX.Element,
    goal: Expression, // TODO generalize?
    given: Expression,
    rules: RuleObj[],
}

export type ReadingScreen = {
    title: string,
    kind: ScreenKind.Reading,
    element: JSX.Element,
}


