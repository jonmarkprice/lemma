import { Dispatch } from 'redux';
import { moveCursor } from '../actions/testActions';
import { Timestamp, GamepadState, StickState, StickID } from '../types/gamepad';

export function scanGamePad(
    now: Timestamp,
    dispatch: Dispatch,
    gamepadState: GamepadState,
) {
    const gamepads = navigator.getGamepads();
    for (const gp of gamepads) {
        if (gp !== null) {
            // scanButtons(gp, gamepadState.buttons, now, dispatch);
            if (gp.axes.length === 4) {
                scanStick(
                    StickID.Left,
                    gp.axes[0],
                    gp.axes[1],
                    gamepadState.sticks[StickID.Left],
                    now,
                    dispatch
                );
                scanStick(
                    StickID.Right,
                    gp.axes[2],
                    gp.axes[3], 
                    gamepadState.sticks[StickID.Right],
                    now,
                    dispatch
                );
            }
        }
    }
}

function scanStick(
    stick: StickID,
    x: number,
    y: number, 
    state: StickState,
    now: Timestamp,
    dispatch: Dispatch
) {
    const DZ = 0.15;
    if (!(Math.abs(x) < DZ && Math.abs(y) < DZ)) {
        if (stick === StickID.Right) {
            dispatch(moveCursor(x,  y, now));
        }
    }
}
