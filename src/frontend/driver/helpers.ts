// These should have no dependency on React so that 
// they will be easier to test.
import {
    ButtonState, ButtonAction, StickAction, Timestamp,
    XboxButton, StickState
} from '../types/gamepad';
import { magnitude, distance } from '../../core/helpers/geometry';

// Constants
const BUTTON_REFIRE_INTERVAL = 500; // microseconds
const RELEASE_TIME = 100;
// const STICK_HOLD_TIME = 100; // microseconds
// const X_SENSITIVITY = 0.05;
// const Y_SENSITIVITY = 0.05;
// const DEAD_ZONE = 0.25;
export const MAGNITUDE_THRESHOLD = 0.25; // unit: closed interval between -1 and 1
const JITTER = 0.3;
const MOVE_COOLDOWN = 300; // a.k.a. refactory period
const STICK_HOLD_WARMUP_INTERVAL = 500; // microseconds
const STICK_HOLD_REFIRE_INTERVAL = 500;
// const AXES_UPDATE_INTERVAL = 1000; // 1K microseconds = 1ms

// See: gamepad ## Last Event
// TODO: rename to buttonReducer
export function scanButton(
    now: Timestamp,
    button: GamepadButton,
    state: ButtonState
): ButtonAction {
    if (state.wasPressed) {
        if (button.pressed) {
            // BUTTON_REFIRE_INTERVAL was REFIRE_TIME
            if (now - state.firstFired >= BUTTON_REFIRE_INTERVAL) {
                // Possibly split use new 'Refire' event, this could increment
                // a 'refireCount' property that would allow for acceleration.
                // To do this we would have to make REFIRE_TIME be a function
                // of the refireCount.
                // Initially a Fire event would set to 0. Each refire would
                // increment.

                // For now, just use Fire which simply resets firstPressed.
                return ButtonAction.Fire;
            } else {
                // Holding the button, update lastFire time
                return ButtonAction.Held;
            }
        } else {
            // Was holding, but no signal this frame. Check for release:
            if (now - state.lastFired >= RELEASE_TIME) {
                return ButtonAction.Release;
            } else {
                // Keep holding until release time or pressed signal signal.
                return ButtonAction.NoOp;
            }
        }
    } else {
        if (button.pressed) {
            return ButtonAction.Fire;
        } else {
            return ButtonAction.NoOp;
        }
    }
}


// Let's try to clear this.
// 1. Capture this algorithm in a .md; commit it working branch
// 2. In a 'stick-vis' branch, make the driver simply store stick data in a
//    in React object -- or just in a ref.
// 3. Write a component that visualizes the values and regions of each for each
//    of the values we are comparing. E.g. 
//      - display an x/y grid for each stick
//      - display the currett position as a point on the grid
//      - draw a box giving the jitter around the current position
// ... this seems like a lot of work

// NEW:
export function stickReducer(
    x: number,
    y: number, 
    state: StickState,
    now: Timestamp,
): StickAction {
    // TODO: rm AXES_UPDATE_INTERVAL
    // TODO: rm StickAction.lastUpdate
    // Let's return states 
    // if (now - state.lastUpdate >= AXES_UPDATE_INTERVAL) {
    //     dispatch(moveStick(x, y, id))
    // }
    // I think something is funky with active / inactive
    // console.log(state.held)
    if (magnitude(x, y) <= MAGNITUDE_THRESHOLD) {
        // in dead zone
        // console.log('in dead zone')
        if (state.wasMoved) {
            return StickAction.Release;
        } else {    
            return StickAction.NoOp;
        }
    } else {
        // console.log('outside dead zone')
        if (state.wasMoved) {
            if (now < state.lastMoved + MOVE_COOLDOWN) {
                // cooldown active
                return StickAction.NoOp;
            } else {
                const dist = distance({x, y}, {x: state.lastX, y: state.lastY});
                // console.log(dist)
                if (dist > JITTER) {
                    // exiting jitter zone
                    return StickAction.Move; // new move
                } else {
                    // remains in jitter zone
                    if (state.isHeld) {
                        // NOTE: we could make HOLD_REFIRE_INTERVAL a function
                        // of a hold fire count to implement acceleration.
                        if (now > state.lastHeld + STICK_HOLD_REFIRE_INTERVAL) {
                            return StickAction.ContinueHold; // refire hold event
                        } else {
                            return StickAction.NoOp; // in held state
                        }
                    } else {
                        if (now > state.lastMoved + STICK_HOLD_WARMUP_INTERVAL) {
                            return StickAction.StartHold; // enter held state (Start Hold)
                        } else {
                            // "charging" hold
                            return StickAction.NoOp;
                        }
                    }
                }
            }
        } else { // inactive
            return StickAction.Move; // first move since release
        }
    }
}

export function getName(index: number): XboxButton | null {
    const mappings = new Map<number, XboxButton>([
        [0, 'A'],
        [1, 'B'],
        [2, 'X'],
        [3, 'Y'],
        [4, 'LB'],
        [5, 'RB'],
        [6, 'LT'],
        [7, 'RT'],
        [8, 'LM'],
        [9, 'RM'],
        [10, 'LSD'],
        [11, 'RSD'],
        [12, 'up'],
        [13, 'down'],
        [14, 'left'],
        [15, 'right']
    ]);
    return mappings.get(index) || null;
}