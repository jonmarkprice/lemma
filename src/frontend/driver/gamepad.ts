import { Dispatch } from 'redux';
import { pressButton, moveStick, moveCursor } from '../actions/controller';
import {
    ButtonState, ButtonAction, StickAction, Timestamp,
    XboxButton, GamepadState, StickState, StickID
} from '../types/gamepad';
import { 
    scanButton, stickReducer, getName, MAGNITUDE_THRESHOLD
} from './helpers';
import { magnitude, calculateDegrees } from '../../core/helpers/geometry';

export function scanGamePad(
    now: Timestamp,
    dispatch: Dispatch,
    gamepadState: GamepadState,
) {
    const gamepads = navigator.getGamepads();
    for (const gp of gamepads) {
        if (gp !== null) {
            scanButtons(gp, gamepadState.buttons, now, dispatch);
            if (gp.axes.length === 4) {
                scanStick(
                    StickID.Left,
                    gp.axes[0],
                    gp.axes[1],
                    gamepadState.sticks[StickID.Left],
                    now,
                    dispatch
                );
                scanStick(
                    StickID.Right,
                    gp.axes[2],
                    gp.axes[3], 
                    gamepadState.sticks[StickID.Right],
                    now,
                    dispatch
                );
            }
        }
    }
}

// Upon further investigation it seems that the controller is incredibly
// sensitive and noisy. I would get for a single "click", often about 
// 1 frame of on, 15 off, 1 on, 15 off, and sometimes a 3rd pair.
// So we need to implement a refire time for release too it seems.
// In other words, we need to make each state change have a minimum.

// TODO: probably fold 'event' state into store.
function scanButtons(
    gp: Gamepad,
    buttonEvents: Record<XboxButton, ButtonState>,
    now: number,
    dispatch: Dispatch,
) {
    for (let i = 0; i <= gp.buttons.length; i++) {
        const label = getName(i);
        if (label !== null) {
            const event = buttonEvents[label];
            const action = scanButton(now, gp.buttons[i], event);
            if (action === ButtonAction.Fire) {
                // Reset all
                event.wasPressed = true;
                event.firstFired = now;
                event.lastFired = now;
                // dispatch(addAction(label))
                dispatch(pressButton(label));
            } else if (action === ButtonAction.Release) {
                // console.log('Releasing')
                event.wasPressed = false;
            } else if (action === ButtonAction.Held) {
                // refresh lastFired
                event.lastFired = now;
            }
            // else no-op (not pressed, or holding but no pressed signal)
        } else {
            // TODO: dispatch(); // invalid event error
        }
    }
}

function scanStick(
    stick: StickID,
    x: number,
    y: number, 
    state: StickState,
    now: Timestamp,
    dispatch: Dispatch
) {
    // TODO: let's rework moveStick into displayStickState
    // make it display ACTIVE / INACTIVE or
    // RELEASED, HELD, MOVED

    // TODO: instead of updating 'state' separately, all of this should be done
    // inside the rootReducer.
    const action : StickAction = stickReducer(x, y, state, now);
    updateStickState(state, action, stick, x, y, now);
    if (stick === StickID.Left && magnitude(x, y) > MAGNITUDE_THRESHOLD) {
        dispatch(moveCursor(x, y, now));
    }
    // if (action !== StickAction.NoOp) {
    //     dispatch(displayStickInfo(stick, state))
    // }
    if (action === StickAction.Move) {
        // could put calculateDegrees up in reducer
        dispatch(moveStick(stick, calculateDegrees(x, -y)))
    }
}

function stickName(stick: StickID): string {
    switch (stick) {
        case StickID.Left: return 'left';
        case StickID.Right: return 'right';
    }
}

function actionName(action: StickAction): string {
    switch (action) {
        case StickAction.ContinueHold:
            return 'CONTINUE HOLD';
        case StickAction.Move:
            return 'MOVE';
        case StickAction.NoOp:
            return 'NO OP';
        case StickAction.Release:
            return 'RELEASE';
        case StickAction.StartHold:
            return 'START HOLD';
    }
}

// --- NOTES FROM REDUCER ---

// Here we probably need a separate timestamp for
// holding, because otherwise (if we don't update the
// lastMoved, we'll get a new move every time).
// So lastMoved will mark the time when the stick entered
// the 'hold region', after the hold timer, we will enter
// the HELD state (isHeld: boolean), and update a 'lastHold' timestamp.

// For release stick, we can choose to either reset both lastX and lastY
// to 0, or can transmit exact coordinates within dead zone.
// For now, we'll set to 0 so that everything is reset to the default
// state.

function updateStickState(
    state: StickState,
    action: StickAction,
    stick: StickID,
    x: number,
    y: number,
    now: Timestamp
) {
    if (action === StickAction.Move) {
        state.lastMoved = now;
        state.lastX = x;
        state.lastY = y;
        state.wasMoved = true;
        state.isHeld = false;
        state.lastHeld = 0;
    } else if (action === StickAction.StartHold) {
        state.wasMoved = true; // guaranteed
        state.isHeld = true;
        state.lastHeld = now;
    } else if (action === StickAction.ContinueHold) {
        state.wasMoved = true; // guaranteed
        state.isHeld = true; // gauranteed
        state.lastHeld = now;
    } else if (action === StickAction.Release) {
        state.lastMoved = 0;
        state.lastX = 0;
        state.lastY = 0;
        state.wasMoved = false;
        state.isHeld = false;
        state.lastHeld = 0;
    }
    // otherwise no op
}