import { Menu, ScreenKind } from '../types/screens';
import { Path } from '../../core/types/path';

export const NAVIGATION_ERROR = Symbol('NAVIGATION ERROR');

export type NavigationError = {
    kind: typeof NAVIGATION_ERROR,
    error: Error,
}

// Rename to walk?
export function navigate(path: Path, root: Menu): Menu | NavigationError {
    let current = root;
    for (let i = 0; i < path.length; i++) {
        // maybe check kind...
        if (current.kind === ScreenKind.Menu) {
            if (path[i] < 0 || path[i] >= current.menu.length) {
                return {
                    kind: NAVIGATION_ERROR,
                    error: new Error('Invalid path index')
                };
            } else {
                current = current.menu[path[i]];
            }
        } else {
            if (i + 1 < path.length) {
                return {
                    kind: NAVIGATION_ERROR,
                    error: new Error('Level should be the last entry in the path.'),
                };
            }
            return current;
        }
    }
    return current;
}
