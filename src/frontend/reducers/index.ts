import { DisplayTree } from '../../core/types/displaytree';
import { setFocus, Direction, applyDirectionalMove, changeFocus, AddressablePoint } from '../../core/display/focus';
import { parse } from '../../domains/algebra/parser/parser';
import { renderExpression } from '../../core/display/tree';
import { Move, Path } from '../../core/types/path';
import {
    PRESS_BUTTON, PressButton, 
    MOVE_LEFT_STICK, MoveLeftStick,
    MOVE_RIGHT_STICK, MoveRightStick,
    MOVE_CURSOR, MoveCursor,
} from '../types/actions';
import { ConnectControllerAction } from '../types/actions';
import { RuleObj } from '../../domains/algebra/rules';
import { Expression, deepEqual, product } from '../../domains/algebra/expression';
import { applyAtPath } from '../../core/evaluation/path';
import { Rule } from '../../core/evaluation/rule';
import { RuleDirection } from '../../core/evaluation/apply';
import { renderLaTeX } from '../../domains/algebra/latex';
import { navigate, NAVIGATION_ERROR } from '../helpers/navigation';
import { ScreenKind } from '../types/screens';
import { Message, MessageKind, NoMessage } from '../types/message';
import { IAA } from '../types/inputAgnosticActions';
import { Lens } from 'monocle-ts';
import { indexArray } from 'monocle-ts/Index/Array';
import { getGamepadSDA } from './gamepad';
import algebra from '../../content/algebra';
import { Menu } from '../types/screens';
import { layoutTree } from '../../core/display/navtree';
import { Point } from '../../core/types/geometry';
import { Ring } from '../../core/display/ring';
import { BoundingBox } from '../../core/display/shapes';
import { CartesianTree } from '../../core/display/cartesianTree';
import { Timestamp } from '../types/gamepad';
import { polarToCartesian, distance, angleFrom, withinRange } from "../../core/helpers/geometry"; 
import { CursorAction, getCursorAction, projectMove, NAV_RING_RADIUS } from "../../core/display/cursor";
import { boolean } from 'fp-ts';

const TRACK_SWITCH_ANIMATION_INTERVAL = 200; // microseconds, // for continuous moves
const TRACK_SWITCH_COOLDOWN = 500; // microseconds // for another track switch
const SELECTION_RADIUS = 2;

// import { Leaf } from '../../core/types/ast';
// import { Empty, Primitive } from '../../utils/copiable';

// maybe make this an enum?
// type Action = MoveRightStick | MoveLeftStick 
//     | DisplayStickInfo | PressButton | ConnectControllerAction 
//     | AddAction;

type Action = GamepadAction | MouseAction | KeyboardAction;

export type GamepadAction = MoveCursor | MoveLeftStick | MoveRightStick | PressButton | ConnectControllerAction;
export type MouseAction = never; // TODO
export type KeyboardAction = never; // TODO

export type State = {
    // Diagram navigation
    readonly expression: Expression,
    readonly tree: DisplayTree,
    readonly focus: Path,
    readonly cursor: Point,
    readonly source: Point,
    readonly ring: Ring<AddressablePoint>,
    readonly lastDiscreteMove: Timestamp,
    readonly xyTree: CartesianTree,
    readonly treeDimensions: BoundingBox,

    // Rules
    readonly rules: RuleObj[]
    readonly ruleIndex: number,

    // steps
    readonly steps: string[],

    // error: string | null,
    readonly message: Message,

    readonly menuPath: number[],
    readonly selectedIndex: number,

    readonly canAdvance: boolean,
    readonly appError: Error | null,
    readonly showCredits: boolean, // deprecate
    readonly modalOpen: boolean,
}

export const MENU_ROOT: Menu = algebra;

// const expr: Expression = new Leaf(new Primitive(0), new Empty());
const EXPR = parse('{* {+ 1 3} {+ 5 3}}');
const TREE = setFocus(renderExpression(EXPR), []);
const [DIM, CARTESIAN_TREE] = layoutTree(TREE, 1);
const [SELECTED, RING] = changeFocus(CARTESIAN_TREE, []);

const init: State = {
    // TODO: expression and tree should be nullable, and set to null.
    expression: EXPR,
    tree: TREE, // new Leaf('--', {selected: true}),
    focus: [],
    rules: [],
    ruleIndex: 0,
    steps: [],
    message: {kind: MessageKind.None},
    menuPath: [],
    selectedIndex: 0,
    canAdvance: false,
    appError: null,
    showCredits: false,
    cursor: SELECTED,
    source: SELECTED,
    ring: RING,
    lastDiscreteMove: 0,
    treeDimensions: DIM,
    xyTree: CARTESIAN_TREE,
    modalOpen: false,
};

const SCALE = 10;

enum InputType {
    Gamepad     = 'InputType/Gamepad',
    Keyboard    = 'InputType/Keyboard',
    Mouse       = 'InputType/Mouse',
    Other       = 'InputType/Other',
}

// XXX: fold into getStateDependentAction
function categorizeInput(action: Action): InputType {
    switch (action.type) {
        case 'PRESS_BUTTON':
            return InputType.Gamepad;
        // case 'CONNECT_CONTROLLER':
        case 'MOVE_LEFT_STICK':
            return InputType.Gamepad;
        case 'MOVE_RIGHT_STICK':
            return InputType.Gamepad;
        case 'MOVE_CURSOR':
            return InputType.Gamepad;
        default:
            return InputType.Other;
    }
}

// const test = {
//     'PRESS_BUTTON': InputType.Gamepad,
//     'CLICK_MOUSE': InputType.Mouse,
// }

// const result = test['PRESS_BUTTON']


// function getKeyboardSDA(state: State, action: KeyboardAction) {
//     throw new Error('Not implemented');
// }
// function getMouseSDA(state: State, action: MouseAction) {
//     throw new Error('Not implemented');
// }

function getInputAgnosticAction(state: State, action: Action): IAA {
    
    // in getStateDependentAction
    const input = categorizeInput(action);
    // May not be able to use this later, TS isn't smart enough ^
    switch (input) {
        case InputType.Gamepad:
            return getGamepadSDA(state, action);
        case InputType.Keyboard:
            //return getKeyboardSDA(state, action);
            throw new Error('Not implemented');
        case InputType.Mouse:
            // return getMouseSDA(state, action);
            throw new Error('Not implemented');
        default:
            return {type: 'NoOp'};
    }
    // now what?
}

const rulesLens = Lens.fromProp<State>()('rules');
// const directionLens = new Lens<RuleObj, RuleDirection>(
//     r => r.direction,
//     d => r => ({...r, direction: d}) // WTF!?
// )
const directionLens = Lens.fromProp<RuleObj>()('direction');
const nthRule = (n: number) => indexArray<RuleObj>().index(n);

const flipRuleDirection = (d: RuleDirection) => {
    if (d === RuleDirection.LTR) {
        return RuleDirection.RTL;
    } else {
        return RuleDirection.LTR;
    }
}

function navigateToParent(state: State): State {
    if (state.menuPath.length === 0) {
        return state; // no op
    } else {
        // should we clear the game state? probably not
        const lastItem = state.menuPath[state.menuPath.length - 1];
        const parentPath = state.menuPath.slice(0, -1);
        return {
            ...state,
            menuPath: parentPath,
            selectedIndex: lastItem,
            canAdvance: false,
        };
    }
}

function navigateToNextLevel(state: State): State {
    const lastScreen = state.menuPath[state.menuPath.length - 1];
    const parentPath = state.menuPath.slice(0, -1);
    // check if lastScreen + 1 exists
    const newLevel = navigate([...parentPath, lastScreen + 1], MENU_ROOT);
    if (newLevel.kind === NAVIGATION_ERROR) {
        // doesn't exist, go up to parent
        return navigateToParent(state);
    } else {
        return navigateTo([...parentPath, lastScreen + 1], state);
    }
}

function navigateTo(path: Path, state: State): State {
    const newScreen = navigate(path, MENU_ROOT);
    if (newScreen.kind === ScreenKind.Level) {
        // open new level
        const tree = setFocus(renderExpression(newScreen.given), []);
        const [dim, xyTree] = layoutTree(tree, 1);
        const [selected, ring] = changeFocus(xyTree, []);
        return {
            ...state,
            menuPath: path,
            selectedIndex: 0,
            tree,
            expression: newScreen.given.copy(),
            xyTree,
            source: selected,
            cursor: selected,
            rules: [...newScreen.rules], // copy necessary?
            canAdvance: false,
            message: {kind: MessageKind.None},
            focus: [],
            treeDimensions: dim,
            ring,
            modalOpen: false,
        };
    } else if (newScreen.kind === ScreenKind.Reading) {
        return {
            ...state,
            menuPath: path,
            selectedIndex: 0,
            canAdvance: false,
            message: {kind: MessageKind.None},
            modalOpen: false,
        }
    } else if (newScreen.kind === ScreenKind.Menu) {
        return {
            ...state,
            menuPath: path,
            selectedIndex: 0,
            canAdvance: false,
            message: {kind: MessageKind.None},
            modalOpen: false,
        }
    } else {
        // we should never get this!
        return {...state, appError: newScreen.error};
    }
}

const ruleIndexLens = Lens.fromProp<State>()('ruleIndex');

// TODO: unit test, move to...
function minimizeByMeasure<A>(elements: A[], measure: (a: A) => number): A {
    if (elements.length === 0) {
        throw new Error('Cannot be empty');
    }
    let min = Number.POSITIVE_INFINITY;
    let minIndex = 0;
    for (let i = 0; i < elements.length; i++) {
        if (measure(elements[i]) < min) {
            min = measure(elements[i]);
            minIndex = i;
        }
    }
    return elements[minIndex];
}

function inputAgnosticReducer(state: State, action: IAA): State {
    const screen = navigate(state.menuPath, MENU_ROOT);
    if (screen.kind === ScreenKind.Level) {
        // applyGameActions [Level]
        switch (action.type) {
            case 'Game/cursor': {
                // we want to do something like the (old) stickReducer
                // Each time we get an event, we should pass up the timestamp
                // Then, compare to lastMoved and ignore if less than some interval,
                // say 500 microseconds.
                if (performance.now() - state.lastDiscreteMove < TRACK_SWITCH_ANIMATION_INTERVAL) {
                    // console.log('ignoring input during animation');
                    return state;
                }

                // console.log(`Got cursor move, (${action.x}, ${action.y}).`)
                const move = {
                    x: withinRange(0, state.treeDimensions.width, state.cursor.x + action.x * SCALE),
                    y: withinRange(0, state.treeDimensions.height, state.cursor.y + action.y * SCALE),
                };

                // export function getCursor(move: Point, source: Point, cursor: Point, ring: Ring<Point>): Point {
                    // maybe these would be stored in the state.

                if (distance(state.source, move) <= NAV_RING_RADIUS) {
                    // return move;
                    return {...state, cursor: move};
                } else {
                    const cursorAction = getCursorAction(move, state.source, state.cursor);
                    const sourceToCursor = angleFrom(state.source, state.cursor);
                    const target = state.ring.query(sourceToCursor); // can save this as ID in state and use .get()
                    if (target === null) {
                        throw new Error('unexpected null');
                        // TODO: We may want to introduce an epsilon parameter, then
                        // deal naturally with this case.
                    }
                    if (cursorAction === CursorAction.INLINE) {
                        // selection should only happen in this case:
                        const newCursor = projectMove(state.source, target.data.coordinates, move);
                        if (distance(newCursor, target.data.coordinates) < SELECTION_RADIUS) {
                            const tree = setFocus(state.tree, target.data.path);
                            // technically, CartesianTree has a built-in focus
                            // maybe we should remove.

                            const [selected, ring] = changeFocus(state.xyTree, target.data.path);
                            return {
                                ...state,
                                tree,
                                cursor: newCursor,
                                focus: target.data.path,
                                ring,
                                source: selected,
                                lastDiscreteMove: performance.now(),
                            };
                        } else {
                            return {...state, cursor: newCursor};
                        }
                    } else {
                        if (performance.now() - state.lastDiscreteMove < TRACK_SWITCH_COOLDOWN) {
                            return state;
                        }

                        const result = (cursorAction === CursorAction.CCW)
                            ? state.ring.left(target.id)
                            : state.ring.right(target.id);
                        if (result === null) {
                            // this should never happen, unless I introduce an epsilon
                            console.log('unexpected null');
                            // return cursor; // don't move cursor
                            return state;
                        } else {
                            // return midpoint(source, result.data);
                            const radius = NAV_RING_RADIUS + 0.2 * distance(state.source, result.data.coordinates);
                            const newCursor = polarToCartesian(state.source, result.angle, radius);
                            
                            // TODO: SET lastDiscreteMove <- action.timestamp
                            return {...state, cursor: newCursor, lastDiscreteMove: action.timestamp};
                        }
                    }
                }

                // const SCALE = 10;

                // const JUMP_THRESHOLD = 0.5;
                // const instantaneousMagnitude = magnitude(action.x, action.y);
                // const instantaneousAngle = calculateDegrees(action.x, action.y);
                // // we can send these to the state...?

                // // I think we should have everything we need here. No, we need
                // // the output of drawTree... I suppose we have it though.
                // // We can call drawTree again here and compute the angles to 
                // // each of the children. Then, if the magnitude is greater than
                // // the threshold, and if the angle is within epsilon of a child,
                // // then update the state to change the selection (discrete event).
                // // Otherwise (if the magnitude is less) we move the cursor.
                // // If the magnitude is below the deadzone threshold, we do
                // // nothing.

                // // Note that we can actually move the whole gamepad reducer
                // // up here if desired.
                // // To perform a discrete [move focus] action, we update the focus
                // // as well as the cursorX and cursorY (to the previously focused)
                 
                // const [box, shapes, xyTree] = drawTree(state.tree);
                // const points = cropSelectedTree(xyTree, state.focus);

                // // TODO: store tree box height, width in state 
                // const EPSILON = 0.2;
                // const TREE_WIDTH = 400;
                // const TREE_HEIGHT = 400;
                // const x = withinRange(0, TREE_WIDTH, state.cursorX + action.x * SCALE);
                // const y = withinRange(0, TREE_HEIGHT, state.cursorY + action.y * SCALE);
                // const cursor: Point = {x, y};
                // const theta = angleFrom(points.selected, cursor); // other order?
                
                // const matches = points.children
                //     .map((pt: Point, index: number): [number, number] => [index, angleFrom(cursor, pt)])
                //     .filter(([index, angle]: [number, number]): boolean => 
                //         Math.abs(theta - angle) <= EPSILON);

                // // We probably want the point, or else the child index.
                // // It is possible that, if children are close togther enough,
                // // or Epsilon is large enough, that there are multiple angles
                // // within EPSILON of theta. In this case, we pick the closest.
                // if (matches.length > 1) {
                //     // TODO
                //     // min predicate

                //     const second = (x: [number, number]): number => {
                //         return x[1];
                //     }
                //     const [index, angle] = minimizeByMeasure<[number, number]>(matches, second);

                //     // TODO: Make function
                //     if (instantaneousMagnitude > JUMP_THRESHOLD) {
                //         // jump to pt.
                //         // NOTE: We need to get new path (from the point...)
                //         // an index suffices
                //         // const index = 1; // TODO: replace
                //         return {
                //             ...state,
                //             cursorX: points.children[index].x,
                //             cursorY: points.children[index].y,
                //             focus: [...state.focus, index + 1],
                //         };
                //     } else {
                //         // only move cursor
                //         return {
                //             ...state,
                //             cursorX: cursor.x,
                //             cursorY: cursor.y,
                //         };
                //     }
                // } else if (matches.length < 1) {
                //     // Another case is that the cursor is not toward any children
                //     // check parent
                //     if (points.parent !== null 
                //         && Math.abs(theta - angleFrom(cursor, points.parent)) < EPSILON
                //         && instantaneousMagnitude > JUMP_THRESHOLD
                //     ) {
                //         return {
                //             ...state,
                //             cursorX: points.parent.x,
                //             cursorY: points.parent.y,
                //             focus: state.focus.slice(0, -1),
                //         }
                //     } else {
                //         // if not close, move cursor but change nothing else
                //         return {
                //             ...state, 
                //             cursorX: cursor.x,
                //             cursorY: cursor.y,
                //         } 
                //     }
                // } else { // if (angles.length === 1) {
                //     // target/extend the line toward one child.
                //     const [index, angle] = matches[0];
                //     if (instantaneousMagnitude > JUMP_THRESHOLD) {
                //         // jump to pt.
                //         // NOTE: We need to get new path (from the point...)
                //         // an index suffices
                //         return {
                //             ...state,
                //             cursorX: points.children[index].x,
                //             cursorY: points.children[index].y,
                //             focus: [...state.focus, index + 1],
                //         };
                //     } else {
                //         // only move cursor
                //         return {
                //             ...state,
                //             cursorX: cursor.x,
                //             cursorY: cursor.y,
                //         };
                //     }
                // }
             
                // // Do we want to hold on to a point? 
                // // if (angles)

                // // find any angles (children) that are within epsilon of theta
                // // angles.filter()
                // // May need to convert negative angles to positive.
                // // Math.min(angles.map(angle => Math.abs(theta - angle)));
                // // TODO: compute angle between two points
            }
            case 'Game/rule/flip': {
                return rulesLens
                    .composeOptional(nthRule(state.selectedIndex))
                    .composeLens(directionLens)
                    .modify(flipRuleDirection)(state);
            }
            case 'Game/rule/apply': {
                const rule: Rule = state.rules[state.ruleIndex].rule;
                const direction = state.rules[state.ruleIndex].direction;
                try {
                    const result = applyAtPath(state.expression, state.focus, rule, direction);
                    const resultTree = setFocus(renderExpression(result), state.focus);
                    const newStep = renderLaTeX(result, state.focus);
                    const [newDim, newCartesianTree] = layoutTree(resultTree, 1);
                    const [selected, newRing] = changeFocus(newCartesianTree, state.focus);
                    if (deepEqual(result, screen.goal)) {
                        const message = {kind: MessageKind.Victory, text: 'Victory!'};
                        return {
                            ...state,
                            expression: result,
                            tree: resultTree,
                            message,
                            steps: [...state.steps, newStep],
                            canAdvance: true,
                            treeDimensions: newDim,
                            ring: newRing,
                            source: selected,
                            cursor: selected,
                            xyTree: newCartesianTree,
                            modalOpen: true,
                        };
                    } else {
                        const message: Message = {kind: MessageKind.None};
                        return {
                            ...state,
                            expression: result,
                            tree: resultTree,
                            message,
                            steps: [...state.steps, newStep],
                            canAdvance: false,
                            treeDimensions: newDim,
                            ring: newRing,
                            source: selected,
                            xyTree: newCartesianTree,
                            modalOpen: false,
                        };
                    }
                } catch (e) {
                    if (e instanceof Error) {
                        console.log(e);
                        return Object.assign({}, state, {
                            message: {kind: MessageKind.Error, text: e.message},
                        });
                    }
                    else {
                        throw e;
                    }
                }
            }
            case 'Game/rule/selection': {
                return ruleIndexLens.set(action.index)(state);
            }
            // DEPRECATE
            // case 'Game/tree': {
            //     return focusTree(action.direction, state);
            // }
            case 'Nav/back': {
                return navigateToParent(state);
            }
            case 'Nav/next': {
                return navigateToNextLevel(state);
            }
        }
    } else if (screen.kind === ScreenKind.Menu) {
        switch (action.type) {
            case 'Nav/next': {
                return navigateToNextLevel(state);
            }
            case 'Nav/back': {
                return navigateToParent(state);
            }
            case 'Menu/select': {
                return {
                    ...state,
                    selectedIndex: action.index,
                }
            }
            case 'Menu/visit': {
                return navigateTo([...state.menuPath, action.index], state);
            }
        }
    } else if (screen.kind === ScreenKind.Reading) {
        switch (action.type) {
            case 'Nav/next': {
                return navigateToNextLevel(state);
            }
            case 'Nav/back': {
                return navigateToParent(state);
            }
        }
    } else {
        return {...state, appError: screen.error};
    }
    return state;
}

function rootReducer(state: State = init, action: Action): State {
    const iaa = getInputAgnosticAction(state, action);
    if (iaa === null) {
        // also potentially deal with appErrors here
        return state;
    } else {
        return inputAgnosticReducer(state, iaa);
    }
}

export default rootReducer;
