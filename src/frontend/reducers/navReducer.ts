import { Point } from "../../core/types/geometry";
import { MoveCursor } from '../actions/testActions';
import { angleFrom, distance, withinRange } from '../../core/helpers/geometry';
import { Ring } from "../../core/display/ring";
import { CursorAction, getCursorAction, projectMove, NAV_RING_RADIUS } from "../../core/display/cursor";
import { polarToCartesian } from "../../core/helpers/geometry"; 
import { Timestamp } from "../types/gamepad";
import { Path } from "../../core/types/path";
import { CartesianTree, cropSelectedTree } from "../../core/display/cartesianTree";
import { Expression } from "../../domains/algebra/expression";
import { DisplayTree } from "../../core/types/displaytree";
import { renderExpression } from '../../core/display/tree';
import { parse } from '../../domains/algebra/parser/parser';
import { changeFocus, setFocus, AddressablePoint } from '../../core/display/focus';
import { layoutTree } from '../../core/display/navtree';
import { BoundingBox } from "../../core/display/shapes";

export const TREE_HEIGHT = 800;
export const TREE_WIDTH = 800;

const TRACK_SWITCH_ANIMATION_INTERVAL = 200; // microseconds, // for continuous moves
const TRACK_SWITCH_COOLDOWN = 500; // microseconds // for another track switch
const SELECTION_RADIUS = 2;

export type TestState = {
    cursor: Point,
    source: Point,
    ring: Ring<AddressablePoint>,
    lastDiscreteMove: Timestamp,
    focus: Path,
    xyTree: CartesianTree,
    expr: Expression,
    tree: DisplayTree,
    treeDimensions: BoundingBox,
};

type Action = MoveCursor;

const focus: Path = []; // TODO // this is the hard part; cause we have to update from
// cursor moves, which means knowing the index!
const expr = parse('{+ {* 5 {+ 4 3}} 2}');
const tree = setFocus(renderExpression(expr), focus);
const [dim, xyTree] = layoutTree(tree, 1);
const [selected, ring] = changeFocus(xyTree, focus);

// I would kinda like to input an expr and focus and get out everything we need
// (points (if needed), selected, tree, ring, etc, etc.)
// obviously within the function we can use setFocus, renderExpression, layoutTree, cropSelectedTree, populateRing, etc.
// but from the outside, I would like to be able to plug in the minimum needed.
// There are basically two cases:
// 1) change expr 
// 2) change focus
// The function I am imagining would be for (2). So it would be called each time the tree focus
// changes. Consequently, it shouldn't need to redraw the tree, etc.

// function treeToNodes(tree: )

const init: TestState = {
    cursor: selected,
    source: selected,
    ring,
    lastDiscreteMove: 0,
    focus: [],
    xyTree,
    expr,
    tree,
    treeDimensions: dim,
}

function reducer(state: TestState = init, action: Action): TestState {
    const SCALE = 10;
    if (action.type === 'MOVE_CURSOR') {
        // we want to do something like the (old) stickReducer
        // Each time we get an event, we should pass up the timestamp
        // Then, compare to lastMoved and ignore if less than some interval,
        // say 500 microseconds.
        if (performance.now() - state.lastDiscreteMove < TRACK_SWITCH_ANIMATION_INTERVAL) {
            return state;
        }

        const move = {
            x: withinRange(0, TREE_WIDTH, state.cursor.x + action.x * SCALE),
            y: withinRange(0, TREE_HEIGHT, state.cursor.y + action.y * SCALE),
        };

        // export function getCursor(move: Point, source: Point, cursor: Point, ring: Ring<Point>): Point {
            // maybe these would be stored in the state.

        if (distance(state.source, move) <= NAV_RING_RADIUS) {
            // return move;
            return {...state, cursor: move};
        } else {
            const cursorAction = getCursorAction(move, state.source, state.cursor);
            const sourceToCursor = angleFrom(state.source, state.cursor);
            const target = state.ring.query(sourceToCursor); // can save this as ID in state and use .get()
            if (target === null) {
                throw new Error('unexpected null');
                // TODO: We may want to introduce an epsilon parameter, then
                // deal naturally with this case.
            }
            if (cursorAction === CursorAction.INLINE) {
                // selection should only happen in this case:
                const newCursor = projectMove(state.source, target.data.coordinates, move);
                if (distance(newCursor, target.data.coordinates) < SELECTION_RADIUS) {
                    const tree = setFocus(state.tree, state.focus);
                    // technically, CartesianTree has a built-in focus
                    // maybe we should remove.

                    const [selected, ring] = changeFocus(state.xyTree, target.data.path);
                    return {
                        ...state,
                        tree,
                        cursor: newCursor,
                        focus,
                        ring,
                        source: selected,
                        lastDiscreteMove: performance.now(),
                    };
                } else {
                    return {...state, cursor: newCursor};
                }
            } else {
                if (performance.now() - state.lastDiscreteMove < TRACK_SWITCH_COOLDOWN) {
                    return state;
                }

                const result = (cursorAction === CursorAction.CCW)
                    ? state.ring.left(target.id)
                    : state.ring.right(target.id);
                if (result === null) {
                    // this should never happen, unless I introduce an epsilon
                    console.log('unexpected null');
                    // return cursor; // don't move cursor
                    return state;
                } else {
                    // return midpoint(source, result.data);
                    const radius = NAV_RING_RADIUS + 0.2 * distance(state.source, result.data.coordinates);
                    const newCursor = polarToCartesian(state.source, result.angle, radius);
                    
                    // TODO: SET lastDiscreteMove <- action.timestamp
                    return {...state, cursor: newCursor, lastDiscreteMove: action.timestamp};
                }
                
            }
        }
        // const cursor = getCursor(move, state.source, state.cursor, state.ring);
        // return {...state, cursor, move};
    }

    // Let's make sure we have a smooth movement first. then uncomment ^
    // if (action.type === 'MOVE_CURSOR') {
    //     const move = {
    //         x: withinRange(0, 200, state.cursor.x + action.x * SCALE),
    //         y: withinRange(0, 200, state.cursor.y + action.y * SCALE),
    //     };
    //     return {...state, cursor: move};
    // }
    return state;
}

export default reducer;