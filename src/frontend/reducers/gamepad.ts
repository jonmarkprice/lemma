import { ScreenKind } from '../types/screens';
import { XboxButton } from '../types/gamepad';
import { State, GamepadAction } from '../reducers/index';
import { navigate, NAVIGATION_ERROR } from '../helpers/navigation';
import { Direction } from '../../core/display/focus';
import { PRESS_BUTTON, MOVE_LEFT_STICK, MOVE_RIGHT_STICK, MOVE_CURSOR } from '../types/actions';
import { IAA } from '../types/inputAgnosticActions';
import { MENU_ROOT } from '../reducers/index';

// TODO: RETURN ACTION
function getDirection(deg: number): Direction {
    if (deg > 315 || deg <= 45) { // right
        return Direction.NextSibling;
    } else if (deg > 45 && deg <= 135) { // up
        return Direction.Parent;
    } else if (deg > 135 && deg <= 225) { // left
        return Direction.PrevSibling;
    } else { // down
        return Direction.FirstChild;
    }
}

// NOTE: I wonder if we could divide action into a sum type of
// the three inputs, then using the type system we could ensure
// that each of the three get*SDA functions deals with all applicable
// actions.
export function getGamepadSDA(state: State, action: GamepadAction): IAA {
    const screen = navigate(state.menuPath, MENU_ROOT);
    // console.log(action)
    // console.log(screen);

    if (screen.kind === NAVIGATION_ERROR) {
        throw new Error('Navigation error');
    } else if (screen.kind === ScreenKind.Level) {
        if (action.type === PRESS_BUTTON) {
            if (action.label === 'A') {
                if (state.canAdvance) {
                    return {
                        type: 'Nav/next',
                    };
                } else {
                    return {
                        type: 'Game/rule/apply'
                    };
                }
            } else if (action.label === 'X') {
                return {
                    type: 'Game/rule/flip'
                };
            } else if (action.label === 'RM') {
                return {
                    type: 'Nav/back'
                };
            }
        // DEPRECATE MOVE_LEFT_STICK in Game, in favor of MOVE_CURSOR
        // possibly change this back once MOVE_CURSOR is complete.
        // } else if (action.type === MOVE_LEFT_STICK) {
        //    return {
        //        type: 'Game/tree',
        //        direction: getDirection(action.degree),
        //    }; 
        } else if (action.type === MOVE_CURSOR) {
            return {
                type: 'Game/cursor',
                x: action.x,
                y: action.y,
                timestamp: action.timestamp,
            }
        } else if (action.type === MOVE_RIGHT_STICK) {
            // Possibly write getCardinalDirection returning up/down/left/right
            // anything less than +/- 45 degrees is ignored
            if (action.degree >= 45 && action.degree <= 135) {
                // up
                if (state.ruleIndex > 0) {
                    return {
                        type: 'Game/rule/selection',
                        index: state.ruleIndex - 1,
                    };
                }
            } else if (action.degree >= 225 && action.degree <= 315) {
                // down
                if (state.ruleIndex < state.rules.length - 1) {
                    return {
                        type: 'Game/rule/selection',
                        index: state.ruleIndex + 1,
                    };
                }
            }
            return { type: 'NoOp' };
        } else {
            // connect controller?
            return { type: 'NoOp' };
        }
    } else if (screen.kind === ScreenKind.Reading) {
        if (action.type === PRESS_BUTTON) {
            if (action.label === 'A') {
                return { type: 'Nav/next' };
            } else if (action.label === 'B') {
                return { type: 'Nav/back' };
            }
        }
        return { type: 'NoOp' };
    } else if (screen.kind === ScreenKind.Menu) {
        if (action.type === PRESS_BUTTON) {
            if (action.label === 'A') {
                return {
                    type: 'Menu/visit',
                    index: state.selectedIndex,
                };
            }
        } else if (action.type === MOVE_LEFT_STICK) {
            // anything less than +/- 45 degrees is ignored
            if (action.degree >= 45 && action.degree <= 135) {
                // up
                return {
                    type: 'Menu/select',
                    index: state.selectedIndex - 1,
                };
            } else if (action.degree >= 225 && action.degree <= 315) {
                // down
                return {
                    type: 'Menu/select',
                    index: state.selectedIndex + 1,
                }
            }
        }
    }
    return { type: 'NoOp' };
}

// TODO: Deprecate NavAction

// // Deprecate
// function handleXboxButtonPress(
//     action: XboxButton,
//     screen: ScreenKind,
//     state: State
// ): NavAction | null {
//     if (screen === ScreenKind.Level) {
//         return null;
//         // TODO: do we want to use 'B' for this or a menu?
//         // return GameAction.Quit
        
//         // this is entirely dependent on the game state.
//         // return GameAction.Solve
//     } else if (screen === ScreenKind.Menu) {
//         if (action === 'A') {
//             return MenuAction.Select;
//         } else if (action === 'B') {
//             return MenuAction.Back;
//         } else {
//             // do nothing
//             return null;
//         }
//     } else if (screen === ScreenKind.Reading) {
//         if (action === 'A') {
//             return ReadingAction.Next;
//         } else if (action === 'B') {
//             return ReadingAction.Exit;
//         } else {
//             // do nothing
//             return null;
//         }
//         // XXX: how to exit? // I feel like it should be a menu
//         // but there is just one option?
//     }
//     return null;
// }