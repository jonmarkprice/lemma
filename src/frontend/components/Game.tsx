import React from 'react';
import RuleList from './RuleList';
import { LevelScreen } from '../types/screens';
import MessageDisplay from './MessageDisplay';
import { State } from '../reducers';
import { useSelector } from 'react-redux';
import '../styles/game.css';
import LeftStick from './controls/game/LeftStick';
import RightStick from './controls/game/RightStick';
import AButton from './controls/game/AButton';
import Menu from './controls/game/Menu';
import XButton from './controls/game/XButton';
import YButton from './controls/game/YButton';
import 'katex/dist/katex.min.css';
import TeX from '@matejmazur/react-katex';
import { renderLaTeX } from '../../domains/algebra/latex';
import TreeDiagram from './TreeDiagram';

interface GameProps {
    screen: LevelScreen,
}

const Game: React.FC<GameProps> = (props) => {
    const expr = useSelector((state: State) => state.expression);
    const focus = useSelector((state: State) => state.focus);
    const modalOpen = useSelector((state: State) => state.modalOpen);
    const math = renderLaTeX(expr, focus);
    // <MessageDisplay />

    return (
        <div id="layers">
            {   modalOpen
                ? (
                    <div id="overlay" className="layer">
                        <div id="shade">   
                        </div>
                        <div id="modal">
                            <MessageDisplay />
                            <div className="controls">
                                <div><AButton label="Next level" /></div>
                                <div><Menu label="Menu" /></div>
                            </div>
                        </div>
                    </div>
                )
                : null
            }
            <div id="game" className="layer">
                <main>
                    <section className="tree-pane">
                        <header>
                            <h1>{props.screen.title}</h1>
                            <section id="prompt" className="pane">
                                {props.screen.prompt}
                            </section>
                        </header>
                        <div id="expression-display">
                            <div id="tree">
                                <TreeDiagram />
                                <TeX block math={math} />
                            </div>
                        </div>
                    </section>
                    <section id="sidebar">
                        <RuleList />
                        <div className="controls">
                            <div><RightStick label="Pick rule" /></div>
                            <div><AButton label="Use" /></div>
                            <div><XButton label="Flip" /></div>
                        </div>
                    </section>
                </main>
                <div id="global-controls" className="controls">
                    <div><LeftStick label="Move around diagram" /></div>
                    <div><YButton label="Open book" /></div>
                </div>
            </div>
        </div>
    );
}

export default Game;

/* OLD
        <div id="game">
            <header>
                <h1>{props.screen.title}</h1>
            </header>
            <main>
                <div id="page">
                    <section id="prompt" className="pane">
                        {props.screen.prompt}
                    </section>

                    <div id="play-area">
                        <section id="tree" className="pane">

                            <div id="tree-controls" className="controls">
                                <div>a</div>
                                <div>b</div>
                                <div>c</div>
                            </div>
                        </section>
                        <div id="book">
                            <section id="rule-selection" className="pane">

                            </section>
                            <div id="rule-selection-controls" className="controls">
                                <div>left stick up</div>
                                <div>left stick down</div>
                            </div>
                            <section id="selected-rule">
                                <textarea>rule tree</textarea>
                            </section>
                            <div id="rule-controls" className="controls">
                                <div>A : apply</div>
                                <div>X : flip</div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <div id="global-controls" className="controls">
                <div>a</div>
                <div>b</div>
                <div>c</div>
            </div>
        </div>
*/