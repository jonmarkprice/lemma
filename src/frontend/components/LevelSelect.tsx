import React from "react";
import { useSelector } from "react-redux";
import { MenuScreen } from "../types/screens";
import { State } from "../reducers";
import LeftStick from './controls/game/LeftStick';
import AButton from './controls/game/AButton';
import '../styles/menu.css';

interface LevelSelectProps {
    screen: MenuScreen,
}

const LevelSelect: React.FC<LevelSelectProps> = (props) => {
    const index = useSelector((s: State) => s.selectedIndex);
    // const selected = index 
    const isSelected = (key: number) => {
        if (index === key) {
            return 'selected';
        } else {
            return '';
        }
    }

    return (
        <div className="page">
            <main>
                <section className="level-select">
                    <h2>{props.screen.title}</h2>
                    <ol>{
                        props.screen.menu.map((el, i) => (
                            <li className={"level-entry  " + isSelected(i)} key={i}>
                                {el.title}
                            </li>
                        ))
                    }</ol>
            </section>
            </main>
            <div id="global-controls" className="controls">
                <div><LeftStick label="Pick level" /></div>
                <div><AButton label="Play" /></div>
            </div>
        </div>
    );
};

export default LevelSelect;
