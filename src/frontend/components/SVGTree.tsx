import React from 'react';
import { Rect, Line, Text, Shape, FilledCircle, IndexedCircle, BoundingBox, OpenCircle } from '../../core/display/shapes';
import { PathRelation } from '../../core/types/path';
import { SELECTED_COLOR, DESELECTED_COLOR, getColor } from '../colors';


function drawLine(line: Line, index: number) {
    // const strokeWidth = (line.selected === PathRelation.Equal) ? 2 : 1.5;
    return <line
        x1={line.x1}
        y1={line.y1}
        x2={line.x2}
        y2={line.y2}
        style={{strokeWidth: 2.5, stroke: line.strokeColor}}
        key={index}
    />;
}

function drawRect(rect: Rect, index: number) {
    // const strokeWidth = (rect.selected === PathRelation.Equal) ? 2 : 1;
    return <rect 
        x={rect.x}
        y={rect.y}
        width={rect.width}
        height={rect.height} 
        style={{fill: rect.fillColor, stroke: rect.strokeColor, strokeWidth: 1}}
        key={index}
    />;
}

function drawText(elem: Text, index: number) {
    //const color = getSelectionColor(elem.selected);
    //const fontWeight = (elem.selected === PathRelation.Equal) ? 'bold' : 'normal';
    return <text
        x={elem.x}
        y={elem.y}
        key={index}
        style={{fill: elem.color}}
    >
        {elem.content}
    </text>
}

// For use with rules where terminals are automatically assigned numbers
function drawIndexedCircle(elem: IndexedCircle, index: number) {
    return <circle
        cx={elem.cx}
        cy={elem.cy}
        r={elem.r}
        key={index}
        style={{fill: getColor(elem.colorIndex)}} // stroke?
    ></circle>
}

function drawFilledCircle(elem: FilledCircle, index: number) {
    // const color = elem.selected ? SELECTED_COLOR : DESELECTED_COLOR;
    return <circle
        cx={elem.cx}
        cy={elem.cy}
        r={elem.r}
        key={index}
        style={{fill: elem.fillColor, stroke: elem.strokeColor}} // stroke?
    ></circle>
}

function drawOpenCircle(elem: OpenCircle, index: number) {
    return <circle
        cx={elem.cx}
        cy={elem.cy}
        r={elem.r}
        key={index}
        style={{stroke: elem.strokeColor, fill: 'none'}} // stroke?
    ></circle>
}

function draw(shape: Shape, key: number): JSX.Element {
    if (shape instanceof Line) {
        return drawLine(shape, key);
    } else if (shape instanceof Rect) {
        return drawRect(shape, key);
    } else if (shape instanceof IndexedCircle) {
        return drawIndexedCircle(shape, key);
    } else if (shape instanceof FilledCircle) {
        return drawFilledCircle(shape, key);
    } else if (shape instanceof OpenCircle) {
        return drawOpenCircle(shape, key);
    } else {
        return drawText(shape, key);
    }
}

interface SVGTreeProps {
    dimensions: BoundingBox,
    elements: Set<Shape>,
    rightPadding?: number,
    bottomPadding?: number,
}

const SVGTree: React.FC<SVGTreeProps> = (props) => {
    const rightPadding = props.rightPadding || 0;
    const bottomPadding = props.bottomPadding || 0;
    return (
        <svg 
            width={props.dimensions.width + rightPadding}
            height={props.dimensions.height + bottomPadding}>
            <g>{ Array.from(props.elements).map(draw) }</g>
        </svg>
    );
}

export default SVGTree;