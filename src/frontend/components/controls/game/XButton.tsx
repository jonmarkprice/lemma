import React from 'react';
import {ReactComponent as Icon} from '../../../../data/icons/X.svg';

const XButton: React.FC<{label: string}> = (props: {label: string}) => (
    <li className='control'>
        <Icon width="40" height="40"/>
        <span className="control-info">{props.label}</span>
    </li>
);

export default XButton;