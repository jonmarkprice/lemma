import React from 'react';

interface OpProps {
    operator: string, // kind
    selected: boolean,
}
  
const Operator: React.FC<OpProps> = (props: OpProps) => {
    // This is a lot of boilerplatey logic. Is there a cleaner way?
    const selected = (props.selected) ? ['selected'] : [];
    const classes = ['operator', ...selected].join(' ');
    return (
        <span className={classes}>
            {props.operator}
        </span>
    );
}

export default Operator;