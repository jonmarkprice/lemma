import React from 'react';
import { State } from '../reducers';
import { useSelector } from 'react-redux';
import Operator from './Operator';
import { Branch } from '../../core/types/ast';
import { DisplayTree } from '../../core/types/displaytree';
import '../styles/navtree.css';
import { PathRelation } from '../../core/types/path';

// TODO: Think of a better name for the top level component
// maybe Sidebar? TreeControl?
const TokenNav: React.FC = () => {
    const tree = useSelector((state: State) => state.tree);
    return (
        <div id='navtree' className='pane'>
            <NavTree tree={tree} />
        </div>
    );
}

interface NavTreeProps {
    tree: DisplayTree,
}

const NavTree: React.FC<NavTreeProps> = (props: NavTreeProps) => {
    const selected = (props.tree.annotation.selected) ? ['selected'] : [];
    if (props.tree instanceof Branch) {
        const classes = ['navnode', ...selected].join(' ');
        return (
            <ul className={classes} >
                <li>
                    <Operator operator={props.tree.kind.value} selected={props.tree.annotation.selected === PathRelation.Equal} />
                </li>
                { props.tree.children.map((child, i) => 
                    (
                        <li className="operand" key={i}>
                            <NavTree tree={child} />
                        </li>
                    ))
                }
            </ul>
        );
    } else { // if (props.tree instanceof Leaf) {
        const classes = ['navnode', 'terminal', ...selected].join(' ');
        return (
            <span className={classes}>
                {props.tree.value.value}
            </span>
        );
    }
}

export default TokenNav;