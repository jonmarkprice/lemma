import React from 'react';
import AButton from './controls/game/AButton';
import BButton from './controls/game/BButton';
import '../styles/reading.css';

// this could also take ReadingScreen type
interface ReadingProps {
    title: string,
    element: JSX.Element,
}

const Reading: React.FC<ReadingProps> = (props: ReadingProps) => {
    return (
        <div className="page">
            <main>
                <article id="reading">
                    <h1>{props.title}</h1>
                    <section>
                        {props.element}
                    </section>
                </article>
            </main>
            <div id="global-controls" className="controls">
                <div><BButton label="Back to menu" /></div>
                <div><AButton label="Next" /></div>
            </div>
        </div>   
    );
}

export default Reading;