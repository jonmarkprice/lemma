import React from 'react';
import GamepadListener from './GamepadListener';
import '../styles/main.css';
//import NavTest from './NavTest';
import PageRouter from './PageRouter';
// import GPTestListener from './GPTestListener';

// TODO: MOVE containers -> containers, sep. from (dumb) components

const App: React.FC = () => {
    // console.log('re-rendered')
    return (
         <GamepadListener>
            <PageRouter />
        </GamepadListener>
    )
}

//  <GamepadListener>
//      <PageRouter />
//  </GamepadListener>
//         <GPTestListener />


export default App;
