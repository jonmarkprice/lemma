import React from 'react';
import '../styles/tree.css';
import { DisplayTree } from '../../core/types/displaytree';
import { Branch, Leaf } from '../../core/types/ast';
import Operator from './Operator';
import { PathRelation } from '../../core/types/path';

interface TreeProps {
  tree: DisplayTree;
}

const Tree: React.FC<TreeProps> = (props: TreeProps) => {
  const selected = (props.tree.annotation.selected === PathRelation.Equal) ? ['selected'] : [];
  if (props.tree instanceof Branch) {
    if (props.tree.arity === 1) { // unary, prefix
      const classes = ['node', ...selected].join(' ');
      return (
        <div className={classes} >
          <Operator
            operator={props.tree.kind.value}
            selected={props.tree.annotation.selected === PathRelation.Equal} />
          <div className="operand">
            <Tree tree={props.tree.children[0]} />
          </div>
        </div>
      )
    } else if (props.tree.arity === 2) { // binary, infix
      // TODO: assuming infix // We may want to support prefix, postfix later
      // This would be stored in kind.
      const classes = ['node', ...selected].join(' ');
      return (
        <div className={classes}>
          <div className="left">
            <Tree tree={props.tree.children[0]} />
          </div>
          <Operator
            operator={props.tree.kind.value}
            selected={props.tree.annotation.selected === PathRelation.Equal} />
          <div className="right">
            <Tree tree={props.tree.children[1]} />
          </div>
        </div>
      );
    } else {
      throw new Error('Unsupported arity');
    }
  } else if (props.tree instanceof Leaf) {
    const classes = ['node', 'terminal', ...selected].join(' ');
    return (
      <div className={classes}>
        {props.tree.value.value}
      </div>
    );
  } else {
    throw new Error('Unsupported sum type');
  }
}

export default Tree;