// aka Rules
// List of expressions with two sides.
import React from 'react';
import { RuleObj } from '../../domains/algebra/rules';
import { useSelector } from 'react-redux'
import { State } from '../reducers';
import '../styles/rules.css';
import SVGTree from './SVGTree';
import { drawRuleTree } from '../../core/display/ruleTree';
import { RuleDirection } from '../../core/evaluation/apply';
// import { renderLaTeX } from '../../domains/algebra/latex';
import TeX from '@matejmazur/react-katex';


interface RuleProps {
    rule: RuleObj,
    selected: boolean,
}

const RuleEntry: React.FC<RuleProps> = (props: RuleProps) => {
    const selected = props.selected ? ['selected'] : [];
    const classes = ['rule', ...selected].join(' ');
    return (
        <div className={classes}>
            <h3>{props.rule.title}</h3>
            <TeX math={props.rule.value} />
        </div>
    );
}

const RuleList: React.FC<{}> = () => {
    const rules = useSelector((state: State) => state.rules);
    const selected = useSelector((state: State) => state.ruleIndex);
    return (
        <ul id="rule-list">
            {rules.map((rule, index) => (
                <li key={index}>
                    <RuleEntry key={index} rule={rule} selected={index === selected} />
                </li>
            ))}
        </ul>
    );
}

export default RuleList;