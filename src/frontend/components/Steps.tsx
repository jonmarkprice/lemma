// This will be like tree, will update with current tree.
import React from 'react';
import { State } from '../reducers';
import { useSelector } from 'react-redux';
import '../styles/steps.css';
import 'katex/dist/katex.min.css';
import TeX from '@matejmazur/react-katex';

const Steps: React.FC = () => {
    const steps = useSelector((state: State) => state.steps);
    return (
        <div id='steps' className='pane'>
            <h3>Steps</h3>
            <ul>{
                steps.map((step, index) => (
                    <li className='tree' key={index}>
                        <TeX math={'= ' + step} />
                    </li>      
                ))
            }</ul>
        </div>
    );
}

export default Steps;