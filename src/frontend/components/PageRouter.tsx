import React from 'react';
import { useSelector } from 'react-redux';
import { navigate } from '../helpers/navigation';
import { MENU_ROOT, State } from '../reducers';
import Game from './Game';
import { ScreenKind } from '../types/screens';
import LevelSelect from './LevelSelect';
import Reading from './Reading';
import '../styles/page.css';

const PageRouter: React.FC = () => {
    const path = useSelector((s: State) => s.menuPath);
    const showCredits = useSelector((s: State) => s.showCredits);
    const currentScreen = navigate(path, MENU_ROOT);
    if (currentScreen.kind === ScreenKind.Level) {
        return <Game screen={currentScreen} />;
    } else if (currentScreen.kind === ScreenKind.Menu) {
        if (showCredits) {
            return (
                <div id="credits">
                    <p>Thanks for playing!</p>

                    <p>Press <kbd>A</kbd> to return to the main menu.</p>
                </div>
            );
        }
        // Menu
        return <LevelSelect screen={currentScreen} />;
    } else if (currentScreen.kind === ScreenKind.Reading) {
        return <Reading title={currentScreen.title} element={currentScreen.element} />;
    } else {
        return (
            <section id="app-error">
                <h1>Something went very very wrong...</h1>
                <pre>
                    {currentScreen.error.message}
                </pre>
            </section>
        )
    }
};

export default PageRouter;