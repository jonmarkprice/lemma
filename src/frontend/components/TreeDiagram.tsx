import React from 'react';
import '../styles/main.css';
import SVGTree from './SVGTree';
import { BoundingBox, Line, Rect, OpenCircle, FilledCircle } from '../../core/display/shapes';
import { PathRelation } from '../../core/types/path';
import { Point } from '../../core/types/geometry';
// import { TestState } from '../reducers/navReducer';
import { State } from '../reducers';
import { useSelector } from 'react-redux';
import { NAV_RING_RADIUS } from '../../core/display/cursor';
import { AddressablePoint } from '../../core/display/focus';
import { Ring } from '../../core/display/ring';
import { CartesianTree } from '../../core/display/cartesianTree';
import { Branch } from '../../core/types/ast';
import { GREYED_OUT, HIGHLIGHT_COLOR, TEXT_COLOR } from '../colors';

function getPoints(xyTree: CartesianTree): Set<Point> {
    const result = new Set<Point>();
    result.add(xyTree.annotation.location);
    if (xyTree instanceof Branch) {
        xyTree.children.map(getPoints).forEach(s => s.forEach(pt => result.add(pt)));
    }
    return result;
}

function getLines(xyTree: CartesianTree): Set<Line> {
    const result = new Set<Line>();
    // result.add(xyTree.annotation.location);
    if (xyTree instanceof Branch) {
        const parentPoint = xyTree.annotation.location;
        // TODO: set color
        for (const child of xyTree.children) {
            const line = new Line(
                parentPoint.x,
                parentPoint.y,
                child.annotation.location.x,
                child.annotation.location.y,
                GREYED_OUT,
            );
            result.add(line);
        
            // recur
            getLines(child).forEach(cl => result.add(cl));
        }
    }
    return result;
}

const TreeDiagram: React.FC = () => {
    const cursor = useSelector<State, Point>(state => state.cursor);
    const source = useSelector<State, Point>(state => state.source);
    const dim = useSelector<State, BoundingBox>(state => state.treeDimensions);
    const xyTree = useSelector<State, CartesianTree>(state => state.xyTree);
    
    const pointRects = Array.from(getPoints(xyTree)).map(pt => {
        if (pt === source) {
            return new FilledCircle(pt.x, pt.y, 5, TEXT_COLOR, TEXT_COLOR);
        }
        return new FilledCircle(pt.x, pt.y, 3, TEXT_COLOR, TEXT_COLOR);
    });

    //const lines = Array.from(getLines(xyTree));

    // The next step is to call drawTree, and render those shapes too
    // but I think this will mess up the alignment as well as duplicate
    // some things.
    // Let's look at how drawTree is generating the shapes and the points.
    // In particular, rather the points correspond to the top-left, center, 
    // or some other part of the shape.

    const elems = new Set([
        ...getLines(xyTree),
        ...pointRects,
        new Line(source.x, source.y, cursor.x, cursor.y, HIGHLIGHT_COLOR),
        new Rect(source.x, source.y, 1, 1, TEXT_COLOR, TEXT_COLOR),
        new Rect(cursor.x, cursor.y, 1, 1, TEXT_COLOR, TEXT_COLOR),
        new OpenCircle(source.x, source.y, NAV_RING_RADIUS, TEXT_COLOR),
    ]);

    return (
        <div id="expression-display">
            <SVGTree dimensions={dim} elements={elems} />
        </div>
    );
}

export default TreeDiagram;
