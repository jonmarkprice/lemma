import React from 'react';
import { useSelector } from 'react-redux';
import { State } from '../reducers';
import { MessageKind } from '../types/message';

const MessageDisplay: React.FC<{}> = () => {
    const msg = useSelector((state: State) => state.message);
    if (msg.kind === MessageKind.Victory) {
        return (
            <div id="message-display" className='pane victory'>
                <p>Success!</p>
                <p>Press <kbd>A</kbd> to go to the next level</p>
            </div>
        );
    } else if (msg.kind === MessageKind.Error) {
        return (
            <div id="message-display" className='pane error'>
                {msg.text}
            </div>
        );
    } else {
       return null;
    }
}

export default MessageDisplay;