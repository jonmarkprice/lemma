import React from 'react';
import { useDispatch } from 'react-redux';
import { scanGamePad } from '../driver/gamepadTester';
import { GamepadState, ButtonState, StickState, StickID, XboxButton } from '../types/gamepad';
import NavTest from '../components/NavTest';

function initGamepadState() : GamepadState {
    const buttonStartState : ButtonState = {
        wasPressed: false,
        firstFired: 0,
        lastFired: 0
    };
    const stickStartState : StickState = {
        lastMoved: 0,
        lastX: 0,
        lastY: 0,
        wasMoved: false,
        isHeld: false,
        lastHeld: 0,
    }

    // possibly abstract into constructor function
    const BUTTON_LABELS: XboxButton[] = ['A', 'B', 'X', 'Y', 'LB', 'RB', 'LT', 'RT', 'LM', 'RM',
        'LSD', 'RSD', 'up', 'down', 'left', 'right'];
    const STICK_IDS: StickID[] = [StickID.Left, StickID.Right]
    type PartialState = {buttons: {[name: string]: ButtonState}, sticks: {[id: string]: StickState}}
    const s : PartialState = {buttons: {}, sticks: {}};
    for (const button of BUTTON_LABELS) {
        s.buttons[button] = Object.assign({}, buttonStartState);
    }
    for (const stick of STICK_IDS) {
        s.sticks[stick] = Object.assign({}, stickStartState)
    }
    return s as GamepadState;
}

const GPTestListener: React.FC = (props) => {
    console.log('gptest listener rerendered')

 
    const requestRef = React.useRef<number>();
    const dispatch = useDispatch();
    const gpState = React.useRef<GamepadState>(initGamepadState());
    const gamepadRef = React.useRef<Gamepad>();

    const animate = (timestamp: number) => {
        scanGamePad(timestamp, dispatch, gpState.current);
        requestRef.current = requestAnimationFrame(animate);
    }

    // Setup
    React.useEffect(() => {
        window.addEventListener("gamepadconnected", (event: Event) => {
            gamepadRef.current = (event as GamepadEvent).gamepad;
        });
        requestRef.current = requestAnimationFrame(animate);
        const idCopy = requestRef.current;
        if (idCopy !== undefined) { // basically map over a Maybe<number>
            return () => cancelAnimationFrame(idCopy);
        }
    }, []); // Make sure this only runs once

    // console.log(`cursor (${gpState.current.sticks.left.lastX}, ${gpState.current.sticks.left.lastY})`);

    return (
        <div id="app">
            <NavTest />
        </div>
    )
}

export default GPTestListener;