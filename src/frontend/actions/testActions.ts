import { Timestamp } from "../types/gamepad";

export interface MoveCursor {
    type: 'MOVE_CURSOR',
    x: number,
    y: number,
    timestamp: Timestamp,
};

export function moveCursor(x: number, y: number, timestamp: Timestamp) {
    return { type: 'MOVE_CURSOR', x, y, timestamp};
}