// DEPRECATED

import {
    ADD_ACTION,
    AddAction,
} from '../types/actionList';

export function addAction(name: string): AddAction {
    return {
        type: ADD_ACTION,
        value: name
    };
}