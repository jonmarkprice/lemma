import {
  DISPLAY_STICK_INFO, DisplayStickInfo,
  MOVE_RIGHT_STICK, MoveRightStick,
  MOVE_LEFT_STICK, MoveLeftStick,
  CONNECT_CONTROLLER, ConnectControllerAction,
  PRESS_BUTTON, PressButton,
  MOVE_CURSOR, MoveCursor,
} from '../types/actions';
import { StickID, StickState, XboxButton } from '../types/gamepad';

export function connectController(gamepad: Gamepad): ConnectControllerAction {
  console.log(`action: connect controller`)
  return {
    type: CONNECT_CONTROLLER,
    gamepad: gamepad,
  };
}

export function pressButton(label: XboxButton): PressButton {
    return {
        type: PRESS_BUTTON,
        label,
    }
}

export function moveStick(stick: StickID, degree: number): MoveLeftStick | MoveRightStick {
    if (stick === StickID.Left) {
        return {
            type: MOVE_LEFT_STICK,
            degree,
        }
    }
    else {
        return {
            type: MOVE_RIGHT_STICK,
            degree
        }
    }
}

export function displayStickInfo(
  stick: StickID,
  state: StickState,
): DisplayStickInfo {
    return {
        type: DISPLAY_STICK_INFO,
        stick,
        state,
    }
}

export function moveCursor(
    x: number,
    y: number,
    timestamp: number
): MoveCursor {
    return {
        type: MOVE_CURSOR,
        x,
        y,
        timestamp,
    };
}