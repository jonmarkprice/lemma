import { PathRelation } from "../core/types/path";

export const HIGHLIGHT_COLOR = '#f38098';
export const TEXT_COLOR = '#ffffff'
export const GREYED_OUT = '#545c74';
export const SELECTED_COLOR = HIGHLIGHT_COLOR;
export const DESELECTED_COLOR = TEXT_COLOR;

const INDEXED_COLORS = [
    '#9bc1fe', // blue
    '#fe6d5e', // red
    '#6af888', // green
    '#f8e45c', // yellow
    '#d075f6', // purple
    '#ffba4b', // orange
    '#31e7be', // teal
    '#fa78c4', // magenta
    '#52d8ee', // cyan
    '#dbff4b', // lime
];

export function getColor(index: number): string {
    if (index > INDEXED_COLORS.length) {
        throw new Error(`Color index: ${index} is out of range`);
    }
    return INDEXED_COLORS[index - 1];
}

function getSelectionColor(selection: PathRelation): string {
    switch (selection) {
        case PathRelation.Divergent: return DESELECTED_COLOR;
        case PathRelation.Equal: return SELECTED_COLOR;
        case PathRelation.Subpath: return DESELECTED_COLOR; // TODO: add 3rd?
        case PathRelation.Superpath: return DESELECTED_COLOR;  
    }
}
