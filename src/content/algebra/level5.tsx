import React from 'react';
import TeX from '@matejmazur/react-katex';

const Lvl5 = (
    <section>
        <p>
            Apply the associative property to change the grouping of
            the parentheses on <TeX>(a + b) + c</TeX> to be <TeX>a + (b + c)</TeX>
        </p>
        <p>Notice how the order of the variables stays the same, but 
            now <TeX>b</TeX> is grouped with <TeX>c</TeX> instead of with <TeX>a</TeX>.
        </p>
    </section>
);

export default Lvl5;