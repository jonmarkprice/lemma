import React from 'react';
import TeX from '@matejmazur/react-katex';

const Lvl2 = (
    <section>
        <p>
            Now, apply the commutative property to
            the inner expression, <TeX>5 + 8</TeX>.
        </p>
    </section>
);

export default Lvl2;