import React from 'react';
import TeX from '@matejmazur/react-katex';

const Lvl5 = (
    <section>
        <p>
            Now let&lsquo;s try it with numbers. Apply the associative property 
            to group 3 with 2.
        </p>
    </section>
);

export default Lvl5;