import { Path, Move } from '../../core/types/path';
import { ScreenKind, Menu } from '../../frontend/types/screens';
import { plus, multiply } from '../../domains/algebra/operators';
import { variable, value, Term, product } from '../../domains/algebra/expression';
import allRules, * as rules from '../../domains/algebra/rules';
import level1 from './level1';
import level2 from './level2'; 
import level3 from './level3';
import level4 from './level4';
import Reading1 from './reading1';
import level5 from './level5';
import reading2 from './reading2';
import level6 from './level6';
import level7 from './level7';

const algebra: Menu = {
    title: 'Algebra',
    kind: ScreenKind.Menu,
    menu: [
        {
            kind: ScreenKind.Reading,
            title: 'The commutative property',
            element: Reading1,
        }, {
            kind: ScreenKind.Level,
            title: 'Commutative property',
            prompt: level1,
            given: plus<Term>(variable('a'), variable('b')),
            goal:  plus<Term>(variable('b'), variable('a')),
            rules: [rules.COMMUTATIVITY_OF_ADDITION],
        }, {
            kind: ScreenKind.Level,
            title: 'Commutative property: Example 1',
            prompt: level2,
            given: plus<Term>(value(3), plus<Term>(value(5), value(8))),
            goal: plus<Term>(value(3), plus<Term>(value(8), value(5))),
            rules: [rules.COMMUTATIVITY_OF_ADDITION],
        }, {
            kind: ScreenKind.Level,
            title: 'Commutative property: Example 2',
            prompt: level3,
            given: plus<Term>(value(3), plus<Term>(value(5), value(8))),
            goal: plus<Term>(plus<Term>(value(5), value(8)), value(3)),
            rules: [rules.COMMUTATIVITY_OF_ADDITION],
        }, {
            kind: ScreenKind.Level,
            title: 'Commutative property: Example 3',
            prompt: level4,
            given: plus<Term>(plus(value(5), value(4)), plus(value(1), plus(value(3), value(2)))),
            goal: plus<Term>(plus(value(1), plus(value(2), value(3))), plus(value(4), value(5))),
            rules: [rules.COMMUTATIVITY_OF_ADDITION],
        }, {
            kind: ScreenKind.Reading,
            title: 'The associative property',
            element: reading2,
        }, {
            kind: ScreenKind.Level,
            title: 'Associative property',
            prompt: level5,
            given: plus(plus(variable('a'), variable('b')), variable('c')),
            goal: plus(variable('a'), plus(variable('b'), variable('c'))),
            rules: [rules.ASSOCIATIVITY_OF_ADDITION],
        }, {
            kind: ScreenKind.Level,
            title: 'Associative property: Example 1',
            prompt: level6, // TODO
            given: plus(plus(value(4), value(3)), value(2)),
            goal: plus(value(4), plus(value(3), value(2))),
            rules: [rules.ASSOCIATIVITY_OF_ADDITION],
        }, {
            kind: ScreenKind.Level,
            title: "Ariela's hard level",
            prompt: level7,
            given: plus(multiply(value(3), plus(product(5, 'x'), variable('x'))), plus(value(7), product(4, 'x'))),
            goal: plus(multiply(plus(plus(value(15), value(3)), value(4)), variable('x')), value(7)),
            rules: [rules.ASSOCIATIVITY_OF_ADDITION, rules.DISTRIBUTIVE_PROPERTY, rules.COMMUTATIVITY_OF_ADDITION]
        }
    ]
}

// (3 * (5x + x)) + (7 + 4x)
// 15x + 3x + 7 + 4x = ((15 + 3) + 4)x + 7

export default algebra;