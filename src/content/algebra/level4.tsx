import React from 'react';

const Lvl4 = (
    <section>
        <p>
            Can you rearrange the expression so that the 
            numbers 1 through 5 are in order?
        </p>
    </section>
);

export default Lvl4;