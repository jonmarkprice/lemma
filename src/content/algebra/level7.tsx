import React from 'react';
import TeX from '@matejmazur/react-katex';

const Lvl7 = (
    <section>
        <p>
            Ariela&lsquo;s Hard Level:
            Simplify! Make it look like:
        </p>
        <TeX>15x + 3x + 7 + 4x = ((15 + 3) + 4)x + 7</TeX>
    </section>
);

export default Lvl7;