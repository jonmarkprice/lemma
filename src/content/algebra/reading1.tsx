import React from 'react';
import TeX from '@matejmazur/react-katex';

const Reading1 = (
    <section>
        <p>
            When we add numbers together, like the number of apples
            in one basket, and the number of oranges in another we
            can count the fruit in any order.
            So if there are five apples and three oranges, we could count
            the 3 oranges first, then the five apples, or the five
            apples, then the three oranges.
            Either way we will get the same number, eight fruit!
        </p>
        <p>
            In symbols, we can say
        </p>
        <TeX block>
            3 + 5 = 5 + 3 = 8
        </TeX>
        <p>
            This was just an example, it doesn't matter what we are counting,
            no matter what the numbers are we can always say that adding
            some number on the right, and some other number on the left will 
            have the same result.
        </p>
        <p>
            Saying <em>"some number"</em> and <em>"some other number"</em> can
            become confusing, so we give them names.
            Often mathematicians use short, one-letter names, like&nbsp;
            <TeX> a</TeX>, <TeX>b</TeX>, <TeX>x</TeX>, or <TeX>y</TeX>.
            Then we can say:
        </p>
        <p>
            For any numbers <TeX>a, b</TeX>, 
        </p>
        <TeX block>a + b = b + a</TeX>
        <p>
            Let's try it. Apply the rule to the expression.
        </p>

    </section>
);

export default Reading1;