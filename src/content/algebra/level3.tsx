import React from 'react';
import TeX from '@matejmazur/react-katex';

const Lvl3 = (
    <section>
        <p>
            Now, apply the commutative property to
            the whole expression, <TeX>3 + (5 + 8)</TeX>.
        </p>
    </section>
);

export default Lvl3;