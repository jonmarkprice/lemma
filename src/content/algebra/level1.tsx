import React from 'react';
import TeX from '@matejmazur/react-katex';

const Lvl1 = (
    <section>
        <p>
            Let's apply the commutative property to
            the expression <TeX>a + b</TeX>.
        </p>
    </section>
);

export default Lvl1;