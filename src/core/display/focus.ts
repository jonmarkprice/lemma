import { DisplayTree, DisplayData } from '../types/displaytree';
import { Move, Path, PathRelation } from '../types/path';
import { mapNodeData, walk } from '../helpers/paths'
import { AST, Leaf } from '../types/ast';
import { Copiable } from '../../utils/copiable';
import { CartesianTree, cropSelectedTree } from './cartesianTree';
import { Point } from '../types/geometry';
import { Ring } from '../display/ring';
import { angleFrom } from '../helpers/geometry';


// TODO: change
function setSelection(d: DisplayData, cmp: PathRelation): DisplayData {
    return new DisplayData(cmp);
}

export type NavState = {
    tree: DisplayTree,
    focus: Path,
};

export function setFocus<K extends Copiable<K>, T extends Copiable<T>>(
    tree: AST<DisplayData, K, T>,
    focus: Path
): AST<DisplayData, K, T> {
    return mapNodeData(setSelection, focus, tree);
}

export enum Direction {
    Parent      = 'PARENT',
    PrevSibling = 'PREV_SIBLING',
    NextSibling = 'NEXT_SIBLING',
    FirstChild  = 'FIRST_CHILD',
}

export function applyDirectionalMove(move: Direction, focus: Path, tree: DisplayTree): NavState | null {
    if (move === Direction.FirstChild) {
        const newPath = [...focus, 1]
        const node = walk(newPath, tree);
        if (node === null) {
            return null;
        } else {
            return {
                tree: mapNodeData(setSelection, newPath, tree),
                focus: newPath,
            };
        }
    } else {
        // Parent, PrevSibling, and NextSibling all require an nonempty path.
        // i.e. we cannot perform them from the root node.
        if (focus.length < 1) {
            return null;
        } else {
            const parentPath = focus.slice(0, -1); // drop last
            if (move === Direction.Parent) {
                return {
                    tree: mapNodeData(setSelection, parentPath, tree),
                    focus: parentPath,
                };
            } else {
                const lastMove = focus[focus.length - 1];
                const parent = walk(parentPath, tree);
                if (parent === null || parent instanceof Leaf) {
                    throw Error('impossible');
                }
                if (move === Direction.PrevSibling) {
                    if (lastMove < 2) {
                        return null; // no previous sibling
                    } else {
                        const newPath = [...parentPath, lastMove - 1];
                        return {
                            tree: mapNodeData(setSelection, newPath, tree),
                            focus: newPath,
                        };
                    }
                } else {
                    // move === Direction.NextSibling
                    if (lastMove < parent.arity) {
                        // TODO: finish.... my brain is falling asleep
                        const newPath = [...parentPath, lastMove + 1];
                        return {
                            tree: mapNodeData(setSelection, newPath, tree),
                            focus: newPath
                        };
                    } else {
                        return null;
                    } 
                }
            }
        }
    }
}


export function moveFocus(move: Move, focus: Path, tree: DisplayTree): NavState | null {    
    if (move === 0) {
        // For parent, check that path is nonempty, then drop the last item
        if (focus.length === 0) {
            return null;
        } else {
            const parentPath = focus.slice(0, -1) // drop last
            
            // if (move <= tree.arity) {
            
            return {
                tree: mapNodeData(setSelection, parentPath, tree),
                focus: parentPath,
            };
        }
    } else {
        const node = walk([...focus, move], tree);
        if (node === null) {
            return null;
        } else {
            return {
                tree: mapNodeData(setSelection, [...focus, move], tree),
                focus: [...focus, move],
                // current: node, assign here
            }
        }
    }
    // TODO: should probably also check arity, or maybe mapNodeData will do that for us?
    // TODO: we need to make sure you can't "walk off the tree", which is happening now.
}

// NOTE: moveFocus and applyDirectionalMove are almost identical. Only one is relative (direction) and the
// other is absolute (Move/index).

export type AddressablePoint = {
    path: Path,
    coordinates: Point,
}

// TODO: move to Ring? or sim.
function populateRing(source: Point, nodes: AddressablePoint[]): Ring<AddressablePoint> {
    const angles = nodes.map(n => angleFrom(source, n.coordinates));
    const ring = new Ring<AddressablePoint>();
    for (let i = 0; i < nodes.length; i++) {
        ring.insert(angles[i], {path: nodes[i].path, coordinates: nodes[i].coordinates});
    }
    return ring;    
}

export function changeFocus(
    xyTree: CartesianTree,
    focus: Path,
): [Point, Ring<AddressablePoint>] {
    // If possible, make cropSelectedTree local only (not exported).
    const treePoints = cropSelectedTree(xyTree, focus);
    // need to make this into Nodes
    // see code below, maybe make that into function


    // I don't the we need an ID here
    // the part about assigning ids is somewhat awkward
    // maybe just assign when we push to a list?
    const selected = {
        path: focus,
        coordinates: treePoints.selected,
    };
    const points = treePoints.children.map((pt, i) => ({
        path: [...focus, i + 1],
        coordinates: pt,
    }));
    if (treePoints.parent !== null) {
        points.push({
            path: focus.slice(0, -1),
            coordinates: treePoints.parent,
        });
    }

    return [selected.coordinates, populateRing(selected.coordinates, points)];

    // perhaps changeFocus should also set:
    // 1. DisplayTree
    // 2. Expr
}