import { sum } from "lodash";
import { Copiable, Empty } from "../../utils/copiable";
import { Branch, Leaf, AST } from "../types/ast";
import { DisplayTree } from "../types/displaytree";
import { PathRelation } from "../types/path";
import { Shape, Rect, Line, BoundingBox } from './shapes';
import { CartesianTree, CartesianTreeData, TreePoints } from "./cartesianTree";
import { Point } from '../types/geometry';
import { HIGHLIGHT_COLOR, TEXT_COLOR } from "../../frontend/colors";
import { NAV_RING_RADIUS } from "./cursor";

const ICON_HEIGHT = 25;
const ICON_WIDTH = 25;
const LEAF_HEIGHT = 25;
const LEAF_WIDTH = 25;
const BRANCH_VERTICAL_SPACING = 45; // may make equal
const BRANCH_HORIZONTAL_SPACING = 40;

class RenderInfo {
    readonly width: number;
    readonly height: number;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }
}

interface SubtreeAggregate {
    boundingBoxes: BoundingBox[],
    branches: number,
    childCoordinates: Point[],
    childTrees: CartesianTree[],
};

export function layoutTree(expr: DisplayTree, scale: number): [BoundingBox, CartesianTree] {
    return layoutSubtree(NAV_RING_RADIUS/2 + 5, NAV_RING_RADIUS/2 + 5, expr, scale);
}

export function drawCursor(x: number, y: number): Shape {
    // console.log('Drawing cursor: ' + x + ', ' + y)
    const CURSOR_SIZE = 4;
    return new Rect(x + CURSOR_SIZE/2, y + CURSOR_SIZE/2, CURSOR_SIZE, CURSOR_SIZE, TEXT_COLOR, TEXT_COLOR);
}

// TODO: how to pass to drawProjection?
// takes three points:
// (0,0): line start
// (u, v): line end
// (x, y): cursor
export function drawProjection(x: number, y: number, points: TreePoints): Shape {
    // const parent = points.parent; // (0, 0)
    // We need parent to compute the angle.
    const selected = points.selected; // (u, v)
    // const cursor = {x, y}; // (x, y)

    const size_a = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    const phi = Math.atan(x/y);
    const psi = Math.atan(selected.x/selected.y);
    const theta = phi - psi;

    const size_c = size_a * Math.cos(theta);

    const p = size_c * Math.sin(psi);
    const q = size_c * Math.cos(psi);

    return new Line(selected.x, selected.y, p, q, HIGHLIGHT_COLOR)
}

function layoutSubtree(
    x: number,
    y: number,
    expr: DisplayTree,
    scale: number, // value between 0 and 1
): [RenderInfo, CartesianTree] {
    if (expr instanceof Branch) {
        if (expr.arity < 1) {
            throw new Error('Invalid Branch: must have at least one operand!')
        }
        const init: SubtreeAggregate = {
            boundingBoxes: [],
            branches: 0,
            childCoordinates: [],
            childTrees: [],
        }
        const fn = (agg: SubtreeAggregate, cur: DisplayTree) => {
            const branchStartY = y + ICON_HEIGHT + (scale * BRANCH_VERTICAL_SPACING);
            const branchStartX = x + sum(agg.boundingBoxes.map(box => box.width))
                + agg.branches * (scale * BRANCH_HORIZONTAL_SPACING);
            const [subtreeBox, childTree] = layoutSubtree(branchStartX, branchStartY, cur, scale);
            const point: Point = {
                x: branchStartX + subtreeBox.width/2,
                y: branchStartY
            };
            const result: SubtreeAggregate = {
                boundingBoxes: [...agg.boundingBoxes, subtreeBox],
                branches: agg.branches + 1,
                childCoordinates: [...agg.childCoordinates, point],
                childTrees: [...agg.childTrees, childTree],
            }
            return result;
        }
        const aggregate = expr.children.reduce(fn, init);
        const boxWidth = sum(aggregate.boundingBoxes.map(box => box.width))
            + (aggregate.branches - 1) * (scale * BRANCH_HORIZONTAL_SPACING);
        const boxHeight = ICON_HEIGHT + (scale * BRANCH_VERTICAL_SPACING)
            + Math.max(...aggregate.boundingBoxes.map(box => box.height));
        const branchOrigin: Point = {x: x + boxWidth/2, y: y + ICON_HEIGHT};
        const box = new RenderInfo(boxWidth + 2*NAV_RING_RADIUS, boxHeight + 2*NAV_RING_RADIUS); // XXX fix this
        const data: CartesianTreeData = new CartesianTreeData(branchOrigin, expr.annotation.selected);
        const tree: CartesianTree = new Branch(new Empty(), data, ...aggregate.childTrees);
        return [box, tree];

    } else {
        return layoutLeaf(x, y, expr.annotation.selected);
    }
}

export function layoutLeaf(
    x: number,
    y: number,
    selected: PathRelation,
): [RenderInfo, CartesianTree] {
    const leafPoint = { x: x + LEAF_WIDTH/2, y: y + LEAF_HEIGHT/2};
    const renderInfo = new RenderInfo(
        LEAF_WIDTH,  // bounding box
        LEAF_HEIGHT, // bounding box
    );
    const tree: CartesianTree = new Leaf(
        new Empty(),
        new CartesianTreeData(leafPoint, selected)
    );
    return [renderInfo, tree]
}
