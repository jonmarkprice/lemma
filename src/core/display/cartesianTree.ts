import { PathRelation, Path } from '../types/path';
import { Copiable, Empty } from "../../utils/copiable";
import { Branch, Leaf, AST } from "../types/ast";
import { walk } from '../helpers/paths';
import { Point } from '../types/geometry';

// TODO: returned from renderTree(?) : CartesianTree -> TreePoints
export type TreePoints = {
    parent: Point | null,
    selected: Point,
    siblings: {
        next: Point | null,
        prev: Point | null,
    },
    children: Point[],
}

export class CartesianTreeData implements Copiable<CartesianTreeData> {
    location: Point;
    selected: PathRelation;

    constructor(point: Point, selected: PathRelation) {
        this.location = point;
        this.selected = selected;
    }

    copy() {
        const point = {x: this.location.x, y: this.location.y};
        return new CartesianTreeData(point, this.selected);
    }
};

export type CartesianTree = AST<CartesianTreeData, Empty, Empty>

// DEPRECATE
// This function basically just returns the Path
// but we have the path from state.focus!
export function findSelected(tree: CartesianTree, aggregatePath: Path = []): [boolean, Path] {
    if (tree.annotation.selected === PathRelation.Equal) {
        return [true, aggregatePath];
    } else if (tree.annotation.selected === PathRelation.Superpath) {
        if (tree instanceof Branch) {
            // recur for each child
            const results = tree.children.map((node, i) =>
                findSelected(node, [...aggregatePath, i + 1])
            ).filter(([found, _]) => found);
            if (results.length === 1) {
                return results[0];
            } else {
                throw new Error('superpath with no equal node');
            }
        } else {
            throw new Error('leaf cannot be superpath');
        }
    } else if (tree.annotation.selected === PathRelation.Divergent) {
        return [false, aggregatePath]
    } else {
        throw new Error('found subpath without hitting equal node')
    }
}

export function cropSelectedTree(tree: CartesianTree, focusedPath: Path): TreePoints {
    if (focusedPath.length === 0) {
        const selected = walk(focusedPath, tree);
        if (selected !== null) {
            if (selected instanceof Branch) {
                return {
                    parent: null,
                    selected: selected.annotation.location,
                    children: selected.children.map(node => node.annotation.location),
                    siblings: {prev: null, next: null},          
                }
            } else {
                return {
                    parent: null,
                    selected: selected.annotation.location,
                    children: [],
                    siblings: {prev: null, next: null},
                }
            }
        } else {
            throw new Error('bad path');
        }
    } else {
        // XXX: need to change the convention of letting 0 = parent, 1 = first child, etc.
        const index = focusedPath[focusedPath.length - 1] - 1; // since moves are offset!
        const parentPath = focusedPath.slice(0, -1);
        const parent = walk(parentPath, tree);
        const selected = walk(focusedPath, tree);
        if (parent === null || parent instanceof Leaf || selected === null) {
            throw new Error('bad tree: impossible');
        }
        // XXX: this is such ugly code!
        const prevSibling = (index >= 1) ? parent.children[index - 1].annotation.location : null;
        const nextSibling = (index + 1 < parent.children.length) ? parent.children[index + 1].annotation.location : null;
        const children = (selected instanceof Branch) ? selected.children.map(node => node.annotation.location) : [];
        return {
            parent: parent.annotation.location,
            selected: selected.annotation.location,
            children: children,
            siblings: {prev: prevSibling, next: nextSibling}
        }
    }
}
