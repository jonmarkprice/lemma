import { Expression, showTerm } from '../../domains/algebra/expression';
import { showOperator } from '../../domains/algebra/operators';
import { DisplayTree, DisplayData,  } from '../types/displaytree';
import { Branch, Leaf } from '../types/ast';
import { Primitive } from '../../utils/copiable';
import { PathRelation } from '../types/path';
// import { Rule, showTemplateNode, TemplateExpression } from '../evaluation/rule';

// TODO: I would like to get rid of top altogether and set everything to
// either subpath or divergent.
export function renderExpression(node: Expression): DisplayTree {
    const selected = new DisplayData(PathRelation.Divergent);
    if (node instanceof Branch) {
        return new Branch<DisplayData, Primitive<string>, Primitive<string>>(
            new Primitive(showOperator(node.kind.operator)),
            selected,
            ...node.children.map(ch => renderExpression(ch))
        );
    } else { //if (node instanceof Leaf) {
        return new Leaf(new Primitive(showTerm(node.value)), selected);
    }
}
