// Basic shapes
export type Shape = Line | Rect | Text | FilledCircle | IndexedCircle | OpenCircle;

// In future, may want to move toward more semantic shapes
// e.g. Operator, Branch(?), Leaf, etc.

// TODO: Probably want to split up into Leaf and OpFrame,
// possibly go further by creating an icon class that takes an operator
// and produces an SVG element.
// We can avoid coupling the operator with the "language" and instead
// provide a list of supported symbols as part of the core/display.
// Then each domain/language can implement a mapping from their operator types
// one of the supported operator icons (in core/display).

export interface BoundingBox {
    width: number,
    height: number,
}

export class Line {
    readonly x1: number;
    readonly y1: number;
    readonly x2: number;
    readonly y2: number;
    readonly strokeColor: string;

    constructor(
        x1: number,
        y1: number,
        x2: number,
        y2: number,
        strokeColor: string,
    ) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.strokeColor = strokeColor;
    }
}

export class Rect {
    readonly x: number;
    readonly y: number;
    readonly width: number;
    readonly height: number;
    readonly fillColor: string;
    readonly strokeColor: string;

    constructor(
        x: number,
        y: number,
        width: number,
        height: number,
        fillColor: string,
        strokeColor: string,
    ) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
    }
}

export class FilledCircle {
    readonly cx: number;
    readonly cy: number;
    readonly r: number;
    // readonly selected: PathRelation;
    readonly fillColor: string;
    readonly strokeColor: string;

    constructor(cx: number, cy: number, r: number, fillColor: string, strokeColor: string) {
        this.cx = cx;
        this.cy = cy;
        this.r = r;
        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
    }
}

export class Text {
    readonly x: number;
    readonly y: number;
    readonly content: string;
    readonly color: string;

    constructor(
        x: number, 
        y: number, 
        content: string, 
        color: string,
    ) {
        this.x = x;
        this.y = y;
        this.content = content;
        this.color = color;
    }
}

export class IndexedCircle {
    readonly cx: number;
    readonly cy: number;
    readonly r: number;
    readonly colorIndex: number;

    constructor(cx: number, cy: number, r: number, colorIndex: number) {
        this.cx = cx;
        this.cy = cy;
        this.r = r;
        this.colorIndex = colorIndex;
    }
}

export class OpenCircle {
    readonly cx: number;
    readonly cy: number;
    readonly r: number; 
    readonly strokeColor: string;

    constructor(cx: number, cy: number, r: number, strokeColor: string) {
        this.cx = cx;
        this.cy = cy;
        this.r = r;
        this.strokeColor = strokeColor;
    }
}