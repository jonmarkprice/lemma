import { standardizeAngle } from '../helpers/geometry';

export type Sector<T> = {
    id: number,
    angle: number,
    // min: number,
    // max: number,
    data: T,
}

// type SectorData = {
//     angle: number,
//     min: number,
//     max: number,
// }

type Node<T> = {
    sector: Sector<T>,
    left: number,
    right: number,
}

// function getData(s: Sector): SectorData {
//     return {
//         angle: s.angle,
//         min: s.min,
//         max: s.max,
//     };
// }

export class Ring<T> {
    nodes: Node<T>[];
    // refs: number[];
    minIndex: number;
    // size: number
    // head: Sector | null
    // readonly EPSILON: number;

    constructor() { // epsilon: number) {
        // this.EPSILON = epsilon;
        this.nodes = [];
        this.minIndex = -1; // ugh, but don't want to change type
        // this.refs = [];
        // this.size = 0;
        // this.head = null;
    }

    // could easily add min() and max()

    insert(angle: number, data: T) {
        const key = standardizeAngle(angle);
        if (this.nodes.length === 0) {
            const sector: Sector<T> = {
                angle: key,
                data,
                id: 0,
                // min: angle - this.EPSILON,
                // max: angle + this.EPSILON,
            }
            const node: Node<T> = {sector, left: 0, right: 0};
            this.nodes.push(node);
            this.minIndex = 0;
            // this.refs.push(0);
        } else {
            // can basically do an insertion sort if we keep refs
            // otherwise, just use the "pointers"
            // that's honestly simpler..
            const newIndex = this.nodes.length;
            const minNode = this.nodes[this.minIndex];
            const maxNode = this.nodes[minNode.left];
            if (key < minNode.sector.angle) {
                // update min
                this.minIndex = newIndex;
                // insert left
                // const maxNodeIndex = minNode.left;
                const sector: Sector<T> = {
                    angle: key,
                    data,
                    id: newIndex,
                    // min: // TODO,
                    // max: // TODO,
                };
                const newNode: Node<T> = {
                    sector,
                    left: maxNode.sector.id,
                    right: minNode.sector.id,
                }
                this.nodes.push(newNode);
                maxNode.right = newIndex;
                minNode.left = newIndex;
            } else {
                // TODO: exact match?
                let current = minNode;
                const startIndex = minNode.sector.id;
                do {
                    if (key === current.sector.angle) {
                        throw new Error('Duplicate angle');
                    }
                    current = this.nodes[current.right];
                } while (key > current.sector.angle
                    && current.sector.id !== startIndex);
                // insert right
                const leftNode = this.nodes[current.left];
                // const rightNode = current; // alias
                const sector: Sector<T> = {
                    angle: key,
                    data,
                    id: newIndex,
                }
                const newNode: Node<T> = {
                    sector,
                    left: leftNode.sector.id,
                    right: current.sector.id,
                }
                this.nodes.push(newNode);
                leftNode.right = newIndex;
                current.left = newIndex;
            }

        }
        // update state
        // implement insertion-sort ... but it's just a list.
        // TODO: deal with duplicate angles
    }

    list(): Sector<T>[] {
        // if we dump refs, we can store a minimum value
        // then jump to it and traverse .left or .right
        // to read in either order
        if (this.nodes.length === 0) {
            return [];
        } else {
            const result = [];
            const startIndex = this.minIndex;
            let current = this.nodes[this.minIndex];
            do {
                result.push(current.sector);
                current = this.nodes[current.right];
            } while (current.sector.id !== startIndex);
            return result;
        }    
        // dump unordered, may be ok.
        // return this.nodes.map(x => x.sector);
    }

    query(angle: number): Sector<T> | null {
        const key = standardizeAngle(angle);
        // I could use refs to make this O(log n) but it should
        // never store enough to make it worth it.
        if (this.nodes.length === 0) {
            return null;
        }
        // compute minimum angle for all.
        // we could technically do this by scanning, updating a min.
        let minDiff = Number.POSITIVE_INFINITY;
        let minIndex = 0;
        for (let i = 0; i < this.nodes.length; i++) {
            const diff = Math.abs(key - this.nodes[i].sector.angle);
            if (diff === 0) {
                // shortcut on exact match
                return this.nodes[i].sector;
            }
            if (diff < minDiff) {
                minDiff = diff;
                minIndex = i;
            }
        }
        return this.nodes[minIndex].sector;
    }

    get(id: number): Sector<T> | null {
        if (id >= 0 && id < this.nodes.length) {
            return this.nodes[id].sector;
        } else {
            return null;
        }
    }

    left(id: number): Sector<T> | null {
        if (id >= 0 && id < this.nodes.length) {
            const index = this.nodes[id].left;
            return this.nodes[index].sector;
        } else {
            return null;
        }
    }

    right(id: number): Sector<T> | null {
        if (id >= 0 && id < this.nodes.length) {
            const index = this.nodes[id].right;
            return this.nodes[index].sector;
        } else {
            return null;
        }
    }
}
