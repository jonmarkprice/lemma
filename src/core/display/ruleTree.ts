import { sum } from "lodash";
import { Branch } from "../types/ast";
import { Constant, TemplateExpression, TemplateNode } from "../evaluation/rule";
import { Shape, Rect, Line, Text, IndexedCircle, BoundingBox } from './shapes';
import { showOperator } from '../../domains/algebra/operators';
import { TEXT_COLOR } from "../../frontend/colors";

const ICON_HEIGHT = 15;
const ICON_WIDTH = 15;
const LEAF_HEIGHT = 15; // DEPRECATE
const LEAF_WIDTH = 15; // DEPRECATE
const LEAF_SIZE = 15;
const ICON_TAIL_LENGTH = 15; // renamed, was BRANCH_STEM_HEIGHT, could also call this "neck"
const SPINE_HEIGHT = 20;
const BRANCH_HORIZONTAL_SPACING = 20;
const TEXT_PADDING_LEFT = -10;
const TEXT_PADDING_TOP = 10;

// We may need to make this a property of the kind: K in AST<_, K, _>
// currently we are using DisplayTree which uses a Primitive<string>
const OP_PADDING_LEFT = 3;
const OP_PADDING_TOP = 13;

class RenderInfo {
    readonly width: number;
    readonly height: number;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }
}

interface SubtreeAggregate {
    // width: number,
    // height: number,
    // spineLength: number,
    elements: Shape[],
    boundingBoxes: BoundingBox[],
    branches: number,
};

export function drawRuleTree(expr: TemplateExpression): [BoundingBox, Set<Shape>] {
    return drawSubtree(0, 0, expr);
}

// TODO: NEED TO HANDLE SELECTION
function drawSubtree(
    x: number,
    y: number,
    expr: TemplateExpression,
): [RenderInfo, Set<Shape>] {
    if (expr instanceof Branch) {
        if (expr.arity < 1) {
            throw new Error('Invalid Branch: must have at least one operand!')
        }
        
        // for each child: draw subtree
        const init: SubtreeAggregate = {
            elements: [],
            boundingBoxes: [],
            branches: 0,
        }
        const fn = (agg: SubtreeAggregate, cur: TemplateExpression) => {
            // draw branch

            // XXX: it seems the SPINE_HEIGHT is not being taken into consideration

            const branchStartY = y + ICON_HEIGHT + ICON_TAIL_LENGTH + SPINE_HEIGHT; // tail = stem?
            const branchStartX = x + sum(agg.boundingBoxes.map(box => box.width))
                + agg.branches * BRANCH_HORIZONTAL_SPACING;
            const [subtreeBox, subtree] = drawSubtree(branchStartX, branchStartY, cur);
            const spine = new Line(
                branchStartX + subtreeBox.width/2,
                branchStartY - SPINE_HEIGHT,
                branchStartX + subtreeBox.width/2,
                branchStartY,
                TEXT_COLOR,
            );
            // const branch = new Line(stemX, branchStartY, subtreeX, subtreeY, expr.annotation.selected);
            const result: SubtreeAggregate = {
                elements: [...agg.elements, spine, ...subtree], // XXX: does subtree need to be a single Group?
                boundingBoxes: [...agg.boundingBoxes, subtreeBox],
                branches: agg.branches + 1,
            }
            return result;
        }
        const aggregate = expr.children.reduce(fn, init);
        const boxWidth = sum(aggregate.boundingBoxes.map(box => box.width)) 
            + (aggregate.branches - 1) * BRANCH_HORIZONTAL_SPACING;
        const boxHeight = ICON_HEIGHT + ICON_TAIL_LENGTH + SPINE_HEIGHT
            + Math.max(...aggregate.boundingBoxes.map(box => box.height));
 
        // draw operator
        const icon = new Rect(
            x + boxWidth/2 - ICON_WIDTH/2,
            y,
            ICON_WIDTH,
            ICON_HEIGHT,
            'none',
            TEXT_COLOR,
        );

        // One option here is a text node, another would be writing a custom shape / path for each
        // operator type. Otherwise we may need to provide custom offsets and text-sizes for each
        // operator.
        const operator = new Text(
            (x + boxWidth/2 - ICON_WIDTH/2) + OP_PADDING_LEFT,
            y + OP_PADDING_TOP,
            showOperator(expr.kind.operator),
            TEXT_COLOR,
        );

        // draw stem
        const stem = new Line(
            x + boxWidth/2,
            y + ICON_HEIGHT,
            x + boxWidth/2,
            y + ICON_HEIGHT + ICON_TAIL_LENGTH,
            TEXT_COLOR,
        );

        const firstChildBB = aggregate.boundingBoxes[0];
        const lastChildBB = aggregate.boundingBoxes[aggregate.boundingBoxes.length - 1];

        // draw bar
        const bar = new Line(
            x + firstChildBB.width/2,
            y + ICON_HEIGHT + ICON_TAIL_LENGTH, // TODO: double check
            x + boxWidth - lastChildBB.width/2,
            y + ICON_HEIGHT + ICON_TAIL_LENGTH,
            TEXT_COLOR,
        );

        const group = new Set([icon, operator, stem, bar, ...aggregate.elements]);
        const box = new RenderInfo(boxWidth, boxHeight);
        return [box, group];

    } else {
        return drawLeaf(x, y, expr.value);
    }
}

export function drawLeaf(
    x: number,
    y: number,
    //value: string,
    node: TemplateNode,
): [RenderInfo, Set<Shape>] {
    const group = new Set<Shape>();

    // The color is determined by TemplateNode 
    if (node instanceof Constant) {
        // draw black, square (with text?)
        group.add(new Rect(
            x + LEAF_SIZE/2,
            y + LEAF_SIZE/2,
            LEAF_SIZE,
            LEAF_SIZE,
            'none',
            TEXT_COLOR,
        ));
        group.add(new Text(
            x + LEAF_SIZE + TEXT_PADDING_LEFT,
            y + TEXT_PADDING_TOP,
            String(node.value),
            TEXT_COLOR,
        ));
    } else {
        group.add(new IndexedCircle(
            x + LEAF_WIDTH/2,
            y + LEAF_HEIGHT/2,
            LEAF_HEIGHT/2,
            node.index,
        ));
    }
    const renderInfo = new RenderInfo( 
        LEAF_SIZE,  // bounding box
        LEAF_SIZE, // bounding box
    );
    return [renderInfo, group] 
}