import { Point } from "../../core/types/geometry";
import { angleFrom, distance, midpoint, isWithinRange, polarToCartesian } from '../../core/helpers/geometry';
import { Ring } from "./ring";

export const NAV_RING_RADIUS = 20;

export function projectMove(source: Point, target: Point, move: Point): Point {
    const theta = angleFrom(source, target);
    const phi   = angleFrom(source, move);
    const delta = distance(source, move);
    const gamma = distance(source, target);
    // console.log(`phi: ${Math.round(radiansToDegrees(phi))}`);

    if (Math.abs(theta - phi) >= Math.PI/2) {
        return {...source};
    }

    const psi = Math.abs(theta - phi);
    const segLen = Math.min(delta * Math.cos(psi), gamma);
    const signX = Math.abs(theta) > Math.PI/2 ? -1 : 1;
    const signY = Math.sign(theta);
    if (Math.abs(theta) > Math.PI/2) {
        const segX = source.x + segLen * signX * Math.cos(Math.PI - Math.abs(theta));
        const segY = source.y + segLen * signY * Math.sin(Math.PI - Math.abs(theta));
        return {x: segX, y: segY};
    } else {
        const segX = source.x + segLen * signX * Math.cos(Math.abs(theta));
        const segY = source.y + segLen * signY * Math.sin(Math.abs(theta));
        return {x: segX, y: segY};
    }
}

// unused in current version
function closestAngle(source: Point, cursor: Point, points: Point[]): Point {
    // compute difference of angles from source to each point.
    // Return the point with the smallest absolute difference.
    // Throw error if points is empty.
    if (points.length < 1) {
        throw new Error('Bad input; points must be nonempty.');
    } else {
        const angleToCursor = angleFrom(source, cursor);
        const closest = points.reduce((agg, pt) => {
            const delta = Math.abs(angleFrom(source, pt) - angleToCursor);
            if (delta < agg.min) {
                return {min: delta, point: pt};
            } else {
                return agg;
            }
        }, {min: Number.POSITIVE_INFINITY, point: source});
        return closest.point;
    }
}

export enum CursorAction {
    INLINE = 'INLINE',
    CW = 'CLOCKWISE',
    CCW = 'COUNTERCLOCKWISE',
}

export function getCursorAction(move: Point, source: Point, cursor: Point): CursorAction {
    const ANGLE_OFFSET = Math.PI/4;
    // const act = angleFrom(source, cursor); // angleFrom(cursor, target); // angle from source to cursor and angle from cursor to target should be equal
    const acm = angleFrom(cursor, move);
    const asc = angleFrom(source, cursor);
    const asm = angleFrom(source, move);
    // console.log(`move (${move.x}, ${move.y}), acm ${acm}, act (= asc) ${asc}, diff ${Math.abs(asc - acm)}`);
    const diff = Math.abs(asc - acm);
    if (diff <= ANGLE_OFFSET || isWithinRange(diff, Math.PI - ANGLE_OFFSET, Math.PI + ANGLE_OFFSET)) {
        return CursorAction.INLINE;
    } else {
        if (asc < asm) { // TODO: double check
            return CursorAction.CW;
        } else {
            return CursorAction.CCW;
        }
    }
}
