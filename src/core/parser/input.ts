const cursor = Symbol("cursor");
const iterator = Symbol("iterator");
const overflow = Symbol("overflow");

export default class Input {
  [iterator]: Iterator<string>; // string
  [overflow]: boolean;
  [cursor]: IteratorResult<string>;

  constructor(s: string) {
    this[iterator] = s[Symbol.iterator]();
    this[overflow] = false;
    this[cursor] = this[iterator].next();
  }

  done(): boolean { // was get
    return this[cursor].done!;
  }

  current(): string {  // was get
    return this[cursor].value;
  }

  overflowed(): boolean { // was get
    return this[overflow];
  }

  read(): void {
    if (!this[cursor].done) {
      this[cursor] = this[iterator].next();
    } else {
      this[overflow] = true;
    }
  }
}
