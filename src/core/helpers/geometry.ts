import { Point } from '../types/geometry';

export function midpoint(source: Point, target: Point): Point {
    return {
        x: (source.x + target.x)/2,
        y: (source.y + target.y)/2,
    }
}

export function distance(p1: Point, p2: Point): number {
    return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
}

export function radiansToDegrees(radians: number): number {
    return radians * 180 / Math.PI;
}

export function magnitude(x: number, y: number): number {
    return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
}

export function calculateDegrees(x: number, y: number): number {
    return (Math.round(Math.atan2(y, x) * 180 / Math.PI) + 360) % 360;
}

// Possibly make "absolute value" / positive angle a separate function
// TODO: unit test!
export function angleFrom(p1: Point, p2: Point): number {
    const angle = Math.atan2(p2.y - p1.y, p2.x - p1.x);
    // if (angle < 0) {
    //     return 2*Math.PI + angle;
    // }
    return angle;
}

export function standardizeAngle(angle: number): number {
    if (angle > 0 && angle < 2*Math.PI) {
        return angle;
    }
    // this can cause a rounding issue
    return (angle + 2*Math.PI) % (2*Math.PI);
}

export function polarToCartesian(center: Point, angle: number, radius: number): Point {
    return {
        x: center.x + radius * Math.cos(angle),
        y: center.y + radius * Math.sin(angle)
    };
}

// not technically geometry
export function withinRange(min: number, max: number, quantity: number): number {
    return Math.max(min, Math.min(quantity, max));
}

export function isWithinRange(value: number, min: number, max: number): boolean {
    if (value >= min && value <= max) {
        return true;
    }
    return false;
}