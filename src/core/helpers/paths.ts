// import { deepCopy } from '../../utils/misc';
import { Copiable } from '../../utils/copiable';
import { AST, Branch, Leaf } from '../types/ast';
import { Path, PathRelation } from '../types/path';

export function mapNodeData<A extends Copiable<A>, K extends Copiable<K>, T extends Copiable<T>>(
    fn: (d: A, r: PathRelation) => A,
    query: Path,
    node: AST<A, K, T>,
    path: Path = [],
): AST<A, K, T> {
  if (node instanceof Branch) {
      return new Branch(
          node.kind, // deep copy?
          fn(node.annotation, comparePaths(path, query)),
          ...node.children.map((child, i) => mapNodeData(
              fn, query, child, [...path, i + 1]
          )),
      );
  } else {
      return new Leaf(
          // deepCopy(node.value),
          node.value, // TODO
          fn(node.annotation, comparePaths(path, query))
      );
  }
}

// Compare up to min(|a|, |b|), 
// if |a| < |b| and not divergent, a is a subpath of b,
// if |a| > |b| and not divergent, a is a superpath of b
// if |a| = |b| and not divergent, a = b
function comparePaths(a: Path, b: Path): PathRelation {
    const minLen = Math.min(a.length, b.length);
    // for.. compare = zip . reduce eq
    for (let i = 0; i < minLen; i++) {
        if (a[i] !== b[i]) {
            return PathRelation.Divergent
        }
    }
    if (a.length === b.length) {
        return PathRelation.Equal;
    } else if (a.length < b.length) {
        return PathRelation.Subpath;
    } else { // |a| > |b|
        return PathRelation.Superpath;
    }
}

// This is ugly, make it recursive
export function walk<A extends Copiable<A>, K extends Copiable<K>, T extends Copiable<T>>(
    path: Path,
    tree: AST<A, K, T>
): AST<A, K, T> | null {
    if (path.length === 0) {
        return tree;
    } else if (tree instanceof Leaf) {
        return null; // leaf and non-empty path
    } else {
        const [step, ...rest] = path;
        if (step === 0) {
            return null; // won't deal with parents here
            // It would be good to differentiate between the two!
            // Maybe Move = DOWN number | UP
        } else {
            if (step <= tree.arity) {
                return walk(rest, tree.children[step - 1]); // another reason to change!
            } else {
                // bad arity
                return null;
            }
        }
    }
    // NOTE: THIS IS TAIL RECURSIVE
}