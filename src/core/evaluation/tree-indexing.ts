import { TreeNode, Token } from './tree-node';
import { applyToRoot } from './apply';
import { Rule } from './rule';
import { AST, Branch } from '../types/ast';

// Using a domain
import { createOperator, showOperator, Operator } from '../../domains/algebra/operators';
import { Expression, showTerm } from '../../domains/algebra/expression';
import { combineAtRoot } from '../../domains/algebra/combine';
import { Copiable, Empty } from '../../utils/copiable';

const LEFT_PAREN: Token = {
  value: '(',
  isTerm: false,
  index: null,
};

const RIGHT_PAREN: Token = {
  value: ')',
  isTerm: false,
  index: null,
};

export class StatefulCounter implements Counter {
  count: number;
  constructor() {
    this.count = -1;
  }

  next() {
    this.count += 1;
    return this.count;
  }
}

export interface Counter {
  next(): number,
}

export function applyRule(expr: Expression, index: number, rule: Rule): Expression {
  const apply = (node: Expression) => applyToRoot(node, rule);
  return transformIndex(expr, index, apply, new StatefulCounter());
}

export function combine(expr: Expression, index: number): Expression {
  return transformIndex(expr, index, combineAtRoot, new StatefulCounter());
}

// TODO: test
export function transformIndex<T extends Copiable<T>>(
  node: AST<Empty, Operator, T>,
  query: number,
  fn: (x: AST<Empty, Operator, T>) => AST<Empty, Operator, T>,
  counter: Counter,
): AST<Empty, Operator, T> {
  const index = counter.next(); // uncond. incr. here
  if (index === query) {
    return fn(node);
  } else {
    if (node instanceof Branch) {
      const op = createOperator<T>(node.kind.operator);
      return op(
        transformIndex(node.children[0], query, fn, counter),
        transformIndex(node.children[1], query, fn, counter)
      );
    } else {
      return node; // or copy?
    }
  }
}

export function numberTreeAndTokens(
  expr: Expression,
  counter: Counter,
): [TreeNode, Token[]] {
  const index = counter.next(); // uncond. incr. here
  if (expr instanceof Branch) {
    const [leftTree, leftTokens] = numberTreeAndTokens(expr.children[0], counter);
    const [rightTree, rightTokens] = numberTreeAndTokens(expr.children[1], counter);
    const value = showOperator(expr.kind.operator);
    const token: Token = {
      value,
      index,
      isTerm: false,
    };
    const node: TreeNode = {
      index: index,
      left: leftTree,
      right: rightTree,
      value,
      parent: null,
    }
    const tokens = [
      LEFT_PAREN,
      ...leftTokens,
      token,
      ...rightTokens,
      RIGHT_PAREN
    ];
    return [node, tokens];
  } else {
    const value = showTerm(expr.value);
    const node: TreeNode = {
      index,
      value,
      left: null,
      right: null,
      parent: null,
    }
    const token: Token = {
      value,
      index,
      isTerm: true
    }
    return [node, [token]]
  }
}
