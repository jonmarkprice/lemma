import { Operator } from '../../domains/algebra/operators';
import { Copiable, Empty } from '../../utils/copiable';
import { AST, Leaf } from '../types/ast';

export type TemplateExpression = AST<Empty, Operator, TemplateNode> 
export type TemplateNode = Terminal | Constant;

export class Constant implements Copiable<Constant> {
    readonly value: number;
    constructor(value: number) {
        this.value = value;
    }

    copy(): Constant {
        return new Constant(this.value);
    }
}

export class Terminal implements Copiable<Terminal> {
    readonly index: number;
    constructor(index: number) {
        this.index = index;
    }
    
    copy(): Terminal {
        return new Terminal(this.index);
    }
}

export class Rule {
    readonly left: TemplateExpression;
    readonly right: TemplateExpression;
    constructor(left: TemplateExpression, right: TemplateExpression) {
        this.left = left;
        this.right = right;
    }
}

export function terminal(index: number): Leaf<Empty, Terminal> {
    return new Leaf(new Terminal(index), new Empty());
}

export function constant(value: number) {
    return new Leaf(new Constant(value), new Empty());
}

// const terminalNames = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

// export function showTemplateNode(node: TemplateNode): string {
//     if (node instanceof Terminal) {
//         return terminalNames[node.index - 1];
//     } else { //if (node instanceof Constant) {
//         return String(node.value);
//     }
// }

