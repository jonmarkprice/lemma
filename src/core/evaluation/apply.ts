// This currently is somewhat coupled with the algebra domain, but
// should be fairly easy to generalize / decouple.
// This is relatively low priority at the moment.

import { Term, Expression, Value, deepEqual, showTerm } from '../../domains/algebra/expression';
import { Operator, createOperator, showOperator } from '../../domains/algebra/operators';
import { Rule, Terminal, Constant, TemplateNode } from './rule';
import { Empty } from '../../utils/copiable';
import { AST, Branch, Leaf } from '../types/ast';

export enum RuleDirection {
  LTR,
  RTL,
}

export function applyDirection(input: Expression, rule: Rule, direction: RuleDirection): Expression {
  const inputStack: AST<Empty, Operator, Term>[] = [];
  const ruleStack: AST<Empty, Operator, TemplateNode>[] = [];
  
  let inputNode = input;
  const variables: Map<number, AST<Empty, Operator, Term>> = new Map();

  // Set direction
  let ruleNode = rule.left; 
  let template = rule.right;
  if (direction === RuleDirection.RTL) {
    ruleNode = rule.right;
    template = rule.left;
  }

  do {
    // if (ruleNode instanceof Terminal) {
    if (ruleNode instanceof Leaf) {
      if (ruleNode.value instanceof Terminal) {
        // TODO: Check for equality in case of repeats (e.g. factoring)
        // In `ab + ac -> a(b + c)`, need to ensure both a's are equal
        const savedSubtree = variables.get(ruleNode.value.index)
        if (savedSubtree !== undefined && !deepEqual(savedSubtree, inputNode)) {
          throw new Error('Does not match previously saved value');
        }
        variables.set(ruleNode.value.index, inputNode); // N.B. pushes to back
      } else { // ruleNode.value instanceof Constant
        if (inputNode instanceof Leaf) {
          if (inputNode.value instanceof Value) {
            if (inputNode.value.value === ruleNode.value.value) {
              // Ok, do nothing
            } else {
              throw new Error(`Rule expects constant of ${ruleNode.value.value}, but got ${inputNode.value.value}.`);
            }
          } else { //if (inputNode.value instanceof Product) {
            throw new Error(`Rule expects constant, but got product ${showTerm(inputNode.value)}`);
          }
        } else { // inputNode instanceof Branch
          throw new Error(`Rule expects a constant but got an expression (of ${showOperator(inputNode.kind.operator)}).`);
        }
      }
    } else { //if (ruleNode instanceof Branch) {
      if (inputNode instanceof Branch) {
        if (inputNode.arity === ruleNode.arity) {
          if (inputNode.kind.operator === ruleNode.kind.operator) {
            // push backwards
            const inputOperands = Array.from(inputNode.children).reverse();
            const ruleOperands = Array.from(ruleNode.children).reverse();
            inputStack.push(...inputOperands);
            ruleStack.push(...ruleOperands);
          } else {
            throw new Error('CannotApply: operator mismatch');
          }
        } else {
          throw new Error('CannotApply: arity mismatch');
        }
      } else {
        throw new Error(`Rule expects expression but got term ${showTerm(inputNode.value)}`);
      }
    }
    // break if we just finished
    if (ruleStack.length === 0) {
      if (inputStack.length !== 0) throw new Error('stack length mismatch');
      break;
    }
    // now we have a non-empty stack
    // update inputNode and ruleNode pointers

    // XXX: for whatever reason, TS doesn't understand that
    // it's non-empty here...
    inputNode = inputStack.pop()!;
    ruleNode = ruleStack.pop()!;
  } while (true); // or just (true) since we break^

  if (inputStack.length > 0) {
    throw new Error('Non-empty input');
  }

  // Now we should have a collection of variables.
  // Let's create the new rule using the output of the rule.
  return buildExpression(template, variables);
}

// Deprecate?
export function applyToRoot(input: Expression, rule: Rule): Expression {
  try {
    return applyDirection(input, rule, RuleDirection.LTR);
  } catch (e) {
    // guard on type of error?
    return applyDirection(input, rule, RuleDirection.RTL);
  }
}

function buildExpression(
  template: AST<Empty, Operator, TemplateNode>,
  variables: Map<number, AST<Empty, Operator, Term>>
): AST<Empty, Operator, Term> {
  if (template instanceof Branch) {
    const fn = createOperator<Term>(template.kind.operator);
    return fn(
      buildExpression(template.children[0], variables),
      buildExpression(template.children[1], variables)
    );
  } else { // Leaf
    if (template.value instanceof Constant) {
      return new Leaf(new Value(template.value.value), new Empty());
    } else { // if (template instanceof Terminal) {
      const subtree = variables.get(template.value.index);
      if (subtree === undefined) {
        throw new Error(`No index ${template.value.index}.`);
      }
      return subtree;
    }
  }
}
