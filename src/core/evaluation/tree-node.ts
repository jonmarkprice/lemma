import { Expression } from '../../domains/algebra/expression';
import { StatefulCounter, numberTreeAndTokens } from './tree-indexing';

export interface TreeNode {
  value: string,
  left: TreeNode | null;
  right: TreeNode | null;
  parent: TreeNode | null;
  index: number,
}

export interface Token {
  value: string,
  isTerm: boolean,
  index: null | number;
}

export function createTreeAndTokens(expr: Expression): [TreeNode, Token[]] {
  const counter = new StatefulCounter();
  const [tree, tokens] = numberTreeAndTokens(expr, counter);
  const withParents = deepCloneTreeNode(tree, x => x);
  return [withParents, tokens];
}

export function getMaxHeight(treeNode: TreeNode, callback: (x0: TreeNode, x1: number) => void = () => {}): number {
  const leftMaxHeight = treeNode.left ? getMaxHeight(treeNode.left, callback) : -1;
  const rightMaxHeight = treeNode.right ? getMaxHeight(treeNode.right, callback) : -1;

  const maxHeight = Math.max(leftMaxHeight, rightMaxHeight) + 1;

  callback(treeNode, maxHeight);

  return maxHeight;
}

export function getMaxHeightToTreeNodes(treeNode: TreeNode): TreeNode[][] {
  const maxHeightToTreeNodes: TreeNode[][] = Array(getMaxHeight(treeNode) + 1).fill(null).map(_ => []);

  getMaxHeight(treeNode, (currentTreeNode, maxHeight) => {
    maxHeightToTreeNodes[maxHeight].push(currentTreeNode);
  });

  return maxHeightToTreeNodes;
}

export function deepCloneTreeNode(treeNode: TreeNode, callback: (x0: TreeNode) => TreeNode): TreeNode {
  const leftClone = treeNode.left ? deepCloneTreeNode(treeNode.left, callback) : null;
  const rightClone = treeNode.right ? deepCloneTreeNode(treeNode.right, callback) : null;

  const ret = callback({
    value: treeNode.value,
    right: rightClone,
    left: leftClone,
    parent: null,
    index: treeNode.index,
  });

  if (leftClone) {
    leftClone.parent = ret;
  }

  if (rightClone) {
    rightClone.parent = ret;
  }

  return ret;
};
