import { Expression, Term } from "../../domains/algebra/expression";
import { applyDirection, RuleDirection } from './apply';
import { createOperator } from "../../domains/algebra/operators";
import { Rule } from '../../core/evaluation/rule';
import { Path } from '../types/path';
import { Branch } from '../types/ast';

// For a generalized n-ary argument version
// function applyToIndex<T extends Copiable<T>>(
//     xs: T[],
//     index: number,
//     fn: (x: T) => T
// ): T[] {
//     const result = [];
//     for (let i = 0; i < xs.length; i++) {
//         if (i === index) {
//             result.push(fn(xs[i].copy()));
//         } else {
//             result.push(xs[i].copy());
//         }
//     }
//     return result;
// }

// Currently, this uses Expression, which is different from AST in that it
// only supports unary and binary operations
// Ideally, we'd use something like src/core/display/focus::moveFocus which
// uses src/core/helpers::walk, but this will require sweeping type changes.
// For now, we'll write a specific function for Expression.
export function applyAtPath(expr: Expression, path: Path, rule: Rule, ruleDirection: RuleDirection): Expression {
    // traverse down path
    // Note the Path type here is different from that in src/core/types/path
    if (path.length === 0) {
        return applyDirection(expr, rule, ruleDirection);
    } else {
        const [n, ...rest] = path;
        // if (expr instanceof Branch) {
        //     const args = applyToIndex(expr.children, n - 1, ch => applyAtPath(ch, rest, rule, ruleDirection))
        //     return createOperator<Term>(expr.kind.operator)(...args);
        if (expr instanceof Branch) {
            if (n === 1) {
                const left = applyAtPath(expr.children[0].copy(), rest, rule, ruleDirection);
                return createOperator<Term>(expr.kind.operator)(left, expr.children[1].copy());
            } else if (n === 2) { //if (first === Direction.Right) {
                const right = applyAtPath(expr.children[1].copy(), rest, rule, ruleDirection);
                return createOperator<Term>(expr.kind.operator)(expr.children[0].copy(), right);
            } else {
                throw new Error('Unsupported arity');
            }
        }
        else {
            return expr.copy();
        } 
    }
}

