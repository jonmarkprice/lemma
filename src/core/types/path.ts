// TODO: THESE DUPLICATE FUNCTIONALITY IN parser/path. Deprecate that.

// We would need, for each type, a number of directions
// and I'm currently planning on treating left/right and down
// differently.
// Maybe that's not an important assumption though.
// We could use integers instead and map to arbitrary enums.

// By convention:
// export const MOVE_UP = 0;
// export const FIRST_ARGUMENT = 1;
// export const SECOND_ARGUMENT = 2;
// ... and so on
export type Move = number; // natural number, where parent = 0, nth argument = n

export type Path = Move[];

export enum PathRelation {
  Equal,
  Subpath,
  Superpath,
  Divergent,
};
