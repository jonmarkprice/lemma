import { AST } from './ast';
import { Primitive, Copiable } from '../../utils/copiable';
import { PathRelation } from './path';

// XXX: was interface but making class to implement copiable
// do we need an interface anywhere?
export class DisplayData implements Copiable<DisplayData> {
    selected: PathRelation;

    constructor(selected: PathRelation) {
        this.selected = selected;
    }

    copy(): DisplayData {
        return new DisplayData(this.selected);
    }
}

export type DisplayTree = AST<DisplayData, Primitive<string>, Primitive<string>>;
