import { Copiable } from "../../utils/copiable";

export type AST<A extends Copiable<A>, K extends Copiable<K>, V extends Copiable<V>>
    = Branch<A, K, V> | Leaf<A, V>;

export class Branch<A extends Copiable<A>, K extends Copiable<K>, V extends Copiable<V>> implements Copiable<Branch<A, K, V>> {
    readonly kind: K; // uniquely identifies operator/expression
    readonly annotation: A; // data that all nodes have
    readonly children: AST<A, K, V>[];
    readonly arity: number;

    constructor(kind: K, annotation: A, ...children: AST<A, K, V>[]) {
        this.kind = kind;
        this.annotation = annotation;
        this.children = children;
        this.arity = children.length;
    }

    copy(): Branch<A, K, V> {
        const children = this.children.map(child => child.copy());
        return new Branch(this.kind.copy(), this.annotation.copy(), ...children);
    }
}

export class Leaf<A extends Copiable<A>, V extends Copiable<V>> implements Copiable<Leaf<A, V>> {
    readonly annotation: A;
    readonly value: V;

    constructor(value: V, annotation: A) {
        this.value = value;
        this.annotation = annotation;
    }

    copy(): Leaf<A, V> {
        return new Leaf(this.value.copy(), this.annotation.copy())
    }
}

// TODO: move this to docs
/// --- APPROACH: INTERFACE ---
// Maybe we need a generic class instead of an interface for an AST.
// Maybe we still have an interface as well... but the idea should be that
// the goal is that:
// 1. A single AST type should support parsing/evaluation of any domain (sets, logic, algebra, etc.)
// 2. A single DisplayTree type should support any domain
// 3. Both AST and DisplayTree (better name? NavTree? Layout?) should implement this general interface/union type.

// DisplayTree should also carry postfix/prefix/etc. info. 
// Maybe a Position enum? Start with infix/prefix/postfix
// add "vertical" if needed for fractions/complement, etc.

// This Tree class could take the form of 'data' on each node.

// Alternatively, we could make an AST class that would be sufficiently
// general such that both any domains & display information can
// be attached to the same tree.

// --- APPROACH: ABSTRACT CLASS ---
// We could use an abstract class, but then we can't construct nodes.
// type Tree<U, B, L, D> = UnaryNode<U, B, L, D> | BinaryNode<U, B, L, D> | LeafNode<L, D>

// // This should be *extended* by both Domain ASTs and Display tree
// export abstract class UnaryNode<U, B, L, D> {
//     abstract readonly operator: U;
//     abstract readonly data: D; // optional
//     abstract readonly operand: Tree<U, B, L, D>;
// }

// export abstract class BinaryNode<U, B, L, D> {
//     abstract readonly operator: B;
//     abstract readonly data: D; // optional
//     abstract readonly left: Tree<U, B, L, D>;
//     abstract readonly right: Tree<U, B, L, D>;
// }

// export abstract class LeafNode<L, D> {
//     abstract readonly value: L;
//     abstract readonly data: D; // optional
// }

// export type Tree<U, B, L, D> = UnaryNode<U, B, L, D> | BinaryNode<U, B, L, D> | LeafNode<L, D>

// --- APPROACH: CONCRETE CLASS WITH DATA FIELD --
// This should be *extended* by both Domain ASTs and Display tree
// export class UnaryNode<U, B, L, D> {
//     readonly operator: U;
//     readonly data: D;
//     readonly operand: Tree<U, B, L, D>;

//     constructor(operator: U, operand: Tree<U, B, L, D>, data: D) {
//         this.operator = operator;
//         this.operand = operand;
//         this.data = data;
//     }
// }

// export class BinaryNode<U, B, L, D> {
//     readonly data: D;
//     readonly operator: B;
//     readonly left: Tree<U, B, L, D>;
//     readonly right: Tree<U, B, L, D>;

//     constructor(operator: B, left: Tree<U, B, L, D>, right: Tree<U, B, L, D>, data: D) {
//         this.operator = operator;
//         this.left = left;
//         this.right = right;
//         this.data = data;
//     }
// }

// export class LeafNode<L, D> {
//     readonly value: L;
//     readonly data: D;

//     constructor(value: L, data: D) {
//         this.value = value;
//         this.data = data;
//     }
// }


