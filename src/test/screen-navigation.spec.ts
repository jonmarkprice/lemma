import algebra from '../content/algebra';
import { navigate } from '../frontend/helpers/navigation';
import { ScreenKind } from '../frontend/types/screens';

describe('test navigate down root menu', () => {
    test('do nothing, root', () => {
        const root = navigate([], algebra);
        if (root.kind === ScreenKind.Menu) {
            expect(root.kind).toEqual('MENU');
            expect(root.title).toEqual('Algebra');
        } else {
            fail();
        }
    });

    test('select topics', () => {
        const topics = navigate([1], algebra);
        if (topics.kind === ScreenKind.Level) {
            expect(topics.kind).toEqual('LEVEL');
            expect(topics.title).toEqual('Commutative property');
        } else {
            fail();
        }
    })
});

// describe('test navigationReducer', () => {
//     test('test simple page navigation action', () => {
//         const menu: MenuScreen = {
//             title: '', kind: ScreenKind.Menu, menu: []
//         };
//         const newPath = navigationReducer(GameAction.Solve, 0, [1], menu);
//         expect(newPath).toEqual([
//             [2]
//         ]);
//     });
// });
