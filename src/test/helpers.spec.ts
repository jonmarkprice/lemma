import { showOperator, ARITHMETIC_OPERATOR } from '../domains/algebra/operators'; 

describe('show operator', () => {
  test('+', () => {
    expect(showOperator(ARITHMETIC_OPERATOR.Addition)).toBe('+');
  });
  test('*', () => {
    expect(showOperator(ARITHMETIC_OPERATOR.Multiplication)).toBe('*');
  });
})