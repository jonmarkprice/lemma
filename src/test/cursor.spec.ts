import { getCursorAction, CursorAction } from "../core/display/cursor";
import { Ring } from '../core/display/ring';
import { angleFrom, midpoint } from '../core/helpers/geometry';
import { Point } from '../core/types/geometry';

describe('some simple cases with target in the first quadrant', () => {
    test('move is exactly 45 degrees off of the target', () => {
        // const t = {x: 50, y: 0};
        const c = {x: 30, y: 20};
        const s = {x: 0, y: 50};
        const m = {x: 50, y: 20};
        const action = getCursorAction(m, s, c);
        expect(action).toBe(CursorAction.INLINE);
    });

    test('move is clockwise', () => {
        // const t = {x: 40, y: 0};
        const c = {x: 20, y: 30};
        const s = {x: 0, y: 60};
        const m = {x: 40, y: 40};
        const action = getCursorAction(m, s, c);
        expect(action).toBe(CursorAction.CW);
    });

    test('move is inline, at accute angle', () => {
        // const t = {x: 40, y: 0};
        const c = {x: 20, y: 30};
        const s = {x: 0, y: 60};
        const m = {x: 40, y: 10};
        const action = getCursorAction(m, s, c);
        expect(action).toBe(CursorAction.INLINE);
    });
});

describe('move is towards source', () => {
    test('cursor, target in Q4, move in Q1, counterclockwise', () => {
        // const t = {x: 0, y: 30};
        const c = {x: 10, y: 20};
        const s = {x: 30, y: 0};
        const m = {x: 40, y: 50};
        const action = getCursorAction(m, s, c);
        expect(action).toBe(CursorAction.CCW);
    });

    test('cursor, target in Q3, move in Q1, inline', () => {
        // const t = {x: 0, y: 70};
        const c = {x: 10, y: 60};
        const s = {x: 20, y: 50};
        const m = {x: 30, y: 0};
        const action = getCursorAction(m, s, c);
        expect(action).toBe(CursorAction.INLINE);
    });
});

// I want to figure out why I'm getting the weird line to the left
// sometimes.
// By process of elimination, we can check 
// 1. whether the distance check is a fault
// 2. whether it's something with ring
// 3. whether it's something with midpoint
// 4. whether it's something with target === move

// One interesting thing is that we are querying the ring
// for both `sourceToCursor` and `asm` (angle from source to move),
// depending on the action. Is this right?
// I think we could use `sourceToCursor` in both cases.
describe('test points used in NavTest', () => {
    const SOURCE = {x: 400, y: 400};
    const POINTS = [
        {x: 80, y: 80},
        {x: 400, y: 80},
        {x: 200, y: 600},
        {x: 80, y: 480},
        {x: 480, y: 400},
    ];

    // let's do one or two from each point.

    test('test point between quadrants 1 and 2', () => {
        const move = {x: 300, y: 110};
        const cursor = {x: 400, y: 100};
        const action = getCursorAction(move, SOURCE, cursor);
        expect(action).toBe(CursorAction.CCW);
    });
    
});

// DEPRECATED TEST: since we merged segmentEnd into reducer
    // const angles = POINTS.map(pt => angleFrom(SOURCE, pt));
    // const ring = new Ring<Point>();
    // for (let i = 0; i < POINTS.length; i++) {
    //     ring.insert(angles[i], POINTS[i]);
    // }

    // // TODO
    // const move = {x: 458, y: 420}; // right angle, clockwise
    // const cursor = {x: 460, y: 400}; // toward point id #4
    
    // // no longer exported
    // const seg = segmentEnd(move, ring, SOURCE, cursor);
    // // should go to midpoint
    // const clockwiseNode = ring.right(4);
    // expect(clockwiseNode).not.toBeNull();
    // expect(clockwiseNode?.data).toEqual({x: 200, y: 600});
    // expect(seg).toEqual({x: 300, y: 500});

// NOTE:
// It would be cool to have a way to draw out the unit tests so we could
// visualize the unit tests. We have three types of objects to draw:
// 1. points
// 2. lines
// 3. angles
// I'm not sure how to draw the angles at present.
// Obviously they would start at the positive x-axis. The radius would be
// incremented from a constant so that they would not overlap.
// We'd also want to display values.
// I could use the regular draw() and Shapes and display as:
// 1. a generated HTML file
// 2. a generated SVG 
// 3. a React component.

// For drawing angle:
// see: https://stackoverflow.com/questions/5736398/how-to-calculate-the-svg-path-for-an-arc-of-a-circle
// and https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths

// Can use:
// function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
//   var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

//   return {
//     x: centerX + (radius * Math.cos(angleInRadians)),
//     y: centerY + (radius * Math.sin(angleInRadians))
//   };
// }

// function describeArc(x, y, radius, startAngle, endAngle){

//     var start = polarToCartesian(x, y, radius, endAngle);
//     var end = polarToCartesian(x, y, radius, startAngle);

//     var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

//     var d = [
//         "M", start.x, start.y, 
//         "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
//     ].join(" ");

//     return d;       
// }