import { AST, Branch, Leaf } from '../core/types/ast';
import { PathRelation } from '../core/types/path';
import { mapNodeData } from '../core/helpers/paths';
import { Primitive } from '../utils/copiable';

describe('test mapNodeData', () => {
    test('test all four functions on a simple map', () => {
        const query = [2, 1, 1];
        const tree: AST<Primitive<string>, Primitive<string>, Primitive<string>> = new Branch(
            new Primitive('++'),
            new Primitive('c'),
            new Branch(
                new Primitive('++'), 
                new Primitive('b'), 
                new Leaf(new Primitive(''), new Primitive('x')),
                new Leaf(new Primitive(''), new Primitive('a'))
            ),
            new Branch(
                new Primitive('++'),
                new Primitive('d'),
                new Branch(
                    new Primitive('!'), 
                    new Primitive('e'),
                    new Branch(
                        new Primitive('++'),
                        new Primitive('g'),
                        new Leaf(
                            new Primitive(''), new Primitive('h')
                        ),
                        new Leaf(
                            new Primitive(''),
                            new Primitive('i')
                        ),
                    ),
                ),
                new Leaf(
                    new Primitive(''), 
                    new Primitive('f')
                ),
            )
        );
        const expected: AST<Primitive<string>, Primitive<string>, Primitive<string>> = new Branch(
            new Primitive('++'),
            new Primitive('green c'),
            new Branch(
                new Primitive('++'),
                new Primitive('black b'),
                new Leaf(
                    new Primitive(''),
                    new Primitive('black x')),
                new Leaf(
                    new Primitive(''), new Primitive('black a')
                ),
            ),
            new Branch(
                new Primitive('++'),
                new Primitive('green d'),
                new Branch(
                    new Primitive('!'), 
                    new Primitive('green e'),
                    new Branch(
                        new Primitive('++'),
                        new Primitive('red g'),
                        new Leaf(
                            new Primitive(''),
                            new Primitive('blue h')
                        ),
                        new Leaf(
                            new Primitive(''),
                            new Primitive('blue i')
                        ),
                    ),
                ),
                new Leaf(
                    new Primitive(''),
                    new Primitive('black f')
                ),
            ),
        );
        const fn = function(x: Primitive<string>, comp: PathRelation) {
            switch (comp) {
                case PathRelation.Divergent: return new Primitive('black ' + x.value);
                case PathRelation.Subpath: return new Primitive('green ' + x.value);
                case PathRelation.Equal: return new Primitive('red ' + x.value);
                default: return new Primitive('blue ' + x.value);
            }
        }
        const colored = mapNodeData(fn, query, tree);
        expect(colored).toEqual(expected);
    })
})


