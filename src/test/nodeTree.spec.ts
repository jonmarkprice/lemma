import { createTreeAndTokens, TreeNode } from '../core/evaluation/tree-node';
import { parse } from '../domains/algebra/parser/parser';

describe('createTree', () => {
  test('simple tree tests', () => {
    const [nodeTree] = createTreeAndTokens(parse('{+ 3 2}'));
    // It's hard to test this API because of the parent pointers
    const expectOp: TreeNode = {
      value: '+',
      parent: null,
      right: null,
      left: null,
      index: 0,
    }; // to establish refs
    expectOp.left = { value: '3', left: null, right: null, parent: expectOp, index: 1 };
    expectOp.right = { value: '2', left: null, right: null, parent: expectOp, index: 2 };
    expect(nodeTree).toEqual(expectOp);
  });
});