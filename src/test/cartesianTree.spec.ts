import { findSelected, CartesianTreeData } from '../core/display/cartesianTree';
import { Branch, Leaf } from '../core/types/ast';
import { PathRelation } from '../core/types/path';
import { Empty } from '../utils/copiable';

// TODO: Deprecate findSelected / entire file
describe('test simple tree', () => {
    test('single node', () => {
        const tree = new Leaf(new Empty(), new CartesianTreeData({x: 0, y: 0}, PathRelation.Equal))
        expect(findSelected(tree, [])).toEqual([true, []]);
    });

    test('simple branch, top', () => {
        const tree = new Branch(
            new Empty(),
            new CartesianTreeData({x: 0, y: 0}, PathRelation.Equal),
            new Leaf(
                new Empty(),
                new CartesianTreeData({x: 0, y: 0}, PathRelation.Subpath)
            ),
            new Leaf(
                new Empty(),
                new CartesianTreeData({x: 0, y: 0}, PathRelation.Subpath)
            ),
        );
        expect(findSelected(tree, [])).toEqual([true, []]);
    });

    test('simple branch, left', () => {
        const tree = new Branch(
            new Empty(),
            new CartesianTreeData({x: 0, y: 0}, PathRelation.Superpath),
            new Leaf(
                new Empty(),
                new CartesianTreeData({x: 0, y: 0}, PathRelation.Equal)
            ),
            new Leaf(
                new Empty(),
                new CartesianTreeData({x: 0, y: 0}, PathRelation.Divergent)
            ),
        );
        expect(findSelected(tree, [])).toEqual([true, [1]]);
    });

    test('simple branch, right', () => {
        const tree = new Branch(
            new Empty(),
            new CartesianTreeData({x: 0, y: 0}, PathRelation.Superpath),
            new Leaf(
                new Empty(),
                new CartesianTreeData({x: 0, y: 0}, PathRelation.Divergent)
            ),
            new Leaf(
                new Empty(),
                new CartesianTreeData({x: 0, y: 0}, PathRelation.Equal)
            ),
        );
        expect(findSelected(tree, [])).toEqual([true, [2]]);
    });

    // TODO: deep nesting
    // test('deep nesting with parse/focusTree', () => {
    //     const expr = parse('{+ {+ {+ {+ 1 2} 3} 4} 5}');
    //     const tree = setFocus(renderExpression(expr), [1, 1, 1]);
    //     const [_1, _2, xyTree] = drawTree(tree);
    //     // TODO: assert that the selection from setFocus is correct.

    //     expect(findSelected(xyTree)).toEqual([true, [1, 1, 1]]);

    // });

    // findSelected basically just returns the Path, right??
    // but we have the path from state.focus!
});