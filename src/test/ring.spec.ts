import { Ring } from '../core/display/ring';
import { angleFrom, standardizeAngle } from '../core/helpers/geometry';
import { Point } from '../core/types/geometry';

describe('empty ring', () => {
    test('getter functions return null or empty list', () => {
        const r = new Ring<{}>();
        expect(r.list()).toEqual([]);
        expect(r.get(0)).toBe(null);
        expect(r.left(3)).toBe(null);
        expect(r.right(7)).toBe(null);
        expect(r.query(Math.PI/2)).toBe(null);
    });

    test('can build ring from empty', () => {
        type TestData = {msg: string};
        const r = new Ring<TestData>();
        expect(r.list()).toEqual([]);

        // insert an element
        const s1 = {
            id: 0,
            angle: Math.PI,
            data: {msg: 'hello'},
        };
        r.insert(s1.angle, s1.data);
        expect(r.get(0)).toEqual(s1);
        expect(r.list()).toEqual([s1]);

        // insert left
        const s2 = {
            id: 1,
            angle: Math.PI/2,
            data: {msg: 'world'},
        }
        r.insert(s2.angle, s2.data);
        expect(r.list()).toEqual([s2, s1]);

        // insert right
        const s3 = {
            id: 2,
            angle: 6*Math.PI/4,
            data: {msg: 'max'},
        }
        r.insert(s3.angle, s3.data);
        expect(r.list()).toEqual([s2, s1, s3]);
    });
});

describe('duplicate angles are not supported', () => {
    test('throw error on duplicate element', () => {
        const r = new Ring<null>();
        r.insert(0, null)
        expect(() => r.insert(2*Math.PI, null)).toThrowError(new Error('Duplicate angle'));
    });

    // TODO: test with more elements.
});

// TODO: test retrieval of nonstandard angle, i.e. not in [0, 2pi].
describe('test retrieval of nonstandard angle', () => {
    test('-pi = pi', () => {
        const r = new Ring<null>();
        r.insert(-Math.PI, null);
        expect(r.get(0)).toEqual({
            id: 0,
            angle: Math.PI,
            data: null,
        });
    });
})

// TODO test query

// TODO: test actual use case
describe('test actual use case', () => {
    test('retrieve by angle', () => {
        const angle = Math.PI;
        const r = new Ring<null>();
        const angles = [Math.PI, Math.PI/2, 3*Math.PI/4, 5*Math.PI/4, Math.PI/4];
        // populate r
        for (let a of angles) {
            r.insert(a, null);
        }
        const q = r.query(angle);
        if (q !== null) {
            expect(r.left(q.id)).toEqual({
                id: 2,
                angle: 3*Math.PI/4,
                data: null,
            }); // some id
        } else {
            fail("shouldn't get null here");
        }
    });

    // test('retrieve by id')
});

// TODO: test a bunch of points
// in particular, the
describe('test angles from points used for testing', () => {
    test('clockwise rotations', () => {
        const SOURCE = {x: 400, y: 400};
        const POINTS = [
            {x: 80, y: 80},
            {x: 400, y: 80},
            {x: 200, y: 600},
            {x: 80, y: 480},
            {x: 480, y: 400},
        ];
        const angles = POINTS.map(pt => angleFrom(SOURCE, pt));
        const ring = new Ring<Point>();
        for (let i = 0; i < POINTS.length; i++) {
            ring.insert(angles[i], POINTS[i]);
        }

        expect(ring.list()).toEqual([
            {
                angle: 0,
                data: {x: 480, y: 400},
                id: 4,
            }, {
                angle: standardizeAngle(angleFrom(SOURCE, {x: 200, y: 600})),
                data: {x: 200, y: 600},
                id: 2,
            }, {
                angle: standardizeAngle(angleFrom(SOURCE, {x: 80, y: 480})),
                data: {x: 80, y: 480},
                id: 3,
            }, {
                angle: standardizeAngle(angleFrom(SOURCE, {x: 80, y: 80})),
                data: {x: 80, y: 80},
                id: 0,
            }, {
                angle: standardizeAngle(angleFrom(SOURCE, {x: 400, y: 80})),
                data: {x: 400, y: 80},
                id: 1
            }
        ]);
        
        // TODO: TRY SOME QUERIES + left/right


        // expect(r.query())
        // let's try a move with the ring
        // (i.e. copy this test into cursor)
    });
});