// import test from 'tape';
import Input from '../core/parser/input';

describe('Input helper', () => {

    test("basic input", () => {
        const input = new Input("Hi");

        // Initial state
        expect(input.current()).toBe('H')
        expect(input.done()).toBe(false);
        input.read();

        expect(input.current()).toBe('i');
        expect(input.done()).toBe(false);
        input.read();

        expect(input.current()).toBe(undefined);
        expect(input.done()).toBe(true);
    });

    test("empty input", () => {
        const input = new Input("");
        expect(input.current()).toBe(undefined);
        expect(input.done()).toBe(true);
    });

    test("input loop", () => {
        // done is maybe not too clear
        const input = new Input("Hello world");
        let content = "";
        while (!input.done()) {
            content += input.current();
            input.read();
        }
        expect(input.done()).toBe(true);
        expect(content).toBe("Hello world");
    });

});