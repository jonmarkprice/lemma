import { applyAtPath } from "../core/evaluation/path";
import * as rules from "../domains/algebra/rules";
import { RuleDirection } from "../core/evaluation/apply";
import { parse } from '../domains/algebra/parser/parser';
import { multiply, plus } from '../domains/algebra/operators';
import { value } from "../domains/algebra/expression";

describe('test applyAtPath', () => {
    test('test a simple nested transformation', () => {
        const expr = parse('{* 2 {+ 1 3}}');
        const rule = rules.COMMUTATIVITY_OF_ADDITION.rule;
        expect(applyAtPath(expr, [2], rule, RuleDirection.LTR))
            .toEqual(
                multiply(value(2), plus(value(3), value(1)))
            );
    });
});