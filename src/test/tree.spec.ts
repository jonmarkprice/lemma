import { renderExpression } from '../core/display/tree';
import { parse } from '../domains/algebra/parser/parser';
import { Branch, Leaf } from '../core/types/ast';
import { Primitive } from '../utils/copiable';
import { DisplayData } from '../core/types/displaytree';
import { PathRelation } from '../core/types/path';

// AST<Empty, Operator, Term>
// 

describe('parse expression and convert to DisplayTree', () => {
  test('simple distribution', () => {
    const input = parse('{* 2 {+ 3 4}}');
    expect(renderExpression(input)).toEqual(
      new Branch(
        new Primitive('*'),
        new DisplayData(PathRelation.Divergent),
        new Leaf(new Primitive('2'), new DisplayData(PathRelation.Divergent)),
        new Branch(
          new Primitive('+'),
          new DisplayData(PathRelation.Divergent),
          new Leaf(new Primitive('3'), new DisplayData(PathRelation.Divergent)),
          new Leaf(new Primitive('4'), new DisplayData(PathRelation.Divergent))
        ),
      )
    )
  });
});
