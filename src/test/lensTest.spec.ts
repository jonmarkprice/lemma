// XXX: This is more a playground. Can delete this later but this
// just validates that the library is installed and working how
// I expect it to.

import { pipe } from 'fp-ts/lib/function';
import { none, some, isSome, match, exists } from 'fp-ts/Option';
import { Lens, Optional } from 'monocle-ts'
import { indexArray, indexReadonlyArray } from 'monocle-ts/lib/Ix';
import { DisplayTree } from '../core/types/displaytree';
import { Path } from '../core/types/path';
import { Expression } from '../domains/algebra/expression';
import { RuleObj } from '../domains/algebra/rules';

describe('test monocle', () => {
    test('test simple lenses', () => {
        type Person = {
            name: string;
            age: number;
        }
        
        const nameLens = new Lens<Person, string>(
            person => person.name,
            name => person => ({...person, name}),
        )
        
        const steve: Person = {name: "Steve", age: 31};

        // lift a function string -> string to Person -> Person
        const jane = nameLens.modify(_ => "Jane")(steve);
        expect(jane).toEqual({name: "Jane", age: 31});
        
        const ageLens = Lens.fromProp<Person>()("age");
        expect(ageLens.get(steve)).toEqual(31);

        // TODO: compose functions
        const harold = pipe(
            steve,
            nameLens.modify(_ => 'Harold'),
            ageLens.modify(age => age * 2),
        )
        expect(harold)
            .toEqual({name: 'Harold', age: 62});
    });
});

// TODO: see unit tests in monocle-ts
// to understand usage
// enum Page {
//     Level = 'LEVEL',
//     Title = 'TITLE_SCREEN',
//     LevelSelect = 'LEVEL_SELECT',
// }

// TODO: move to core/types

type Screen = GameState | LevelSelect | TitleScreen;

type GameState = {
    page: 'GAME', //Page.Level,
    expression: Expression,
    tree: DisplayTree,
    focus: Path,
    rules:  RuleObj[],
    ruleIndex: number,
    steps: string[],
    error: string | null,
}

//// discriminated union
type LevelSelect = {
    page: 'LEVEL_SELECT', //Page.LevelSelect,
    itemIndex: number,
}

enum Menu {
    Challenges = 'CHALLENGES',
    Topics = 'TOPICS',
}

type TitleScreen = {
    page: 'TITLE_SCREEN', // Page.Title,
    menu: Menu,
    itemIndex: number,
}
////

type State = {
    screen: Screen,
    saves: {},
    progress: {},
}

function nextItem(s: State): State {
    const itemLens: Lens<TitleScreen, number>
        = Lens.fromProp<TitleScreen>()('itemIndex');


    const titleScreenLens = new Optional<State, TitleScreen>(
        (s: State) => (s.screen.page === 'TITLE_SCREEN') ? some(s.screen) : none,
        (t: TitleScreen) => (s: State) => ({...s, screen: t}),
    )

    // const screenLens: Lens<State, Screen> = Lens.fromProp<State>()('screen');

    const indexLens = titleScreenLens.compose(itemLens.asOptional());

    // match(() => )
    return indexLens.modify(n => n + 1)(s);
}

describe('state test using lenses', () => {
    test('title screen navigation', () => {
        // TODO: don't import reducer for now
        // just code own reducer.
        const state: State = {
            screen: {
                page: 'TITLE_SCREEN',
                menu: Menu.Challenges,
                itemIndex: 0,
            },
            saves: {},
            progress: {},
        }

        const itemLens: Lens<TitleScreen, number>
            = Lens.fromProp<TitleScreen>()('itemIndex');

        // Maybe we should map from <State, TitleScreen>, <State, Game

        // option?
        // const titleScreen: Lens<Screen, TitleScreen>
        // Need an optional, 
        // Screen maybe any of TitleScreen, LevelSelect, or GameState
        const titleScreenLens = new Optional<State, TitleScreen>(
            (s: State) => (s.screen.page === 'TITLE_SCREEN') ? some(s.screen) : none,
            (t: TitleScreen) => (s: State) => ({...s, screen: t}),
        );

        // const screenLens: Lens<State, Screen> = Lens.fromProp<State>()('screen');

        const indexLens = titleScreenLens.compose(itemLens.asOptional());
        // expect(isSome(indexLens.getOption(state))).toBe(true);
        // expect(match(() => false, index => index === 0)(indexLens.getOption(state))).toBe(true);
        expect(exists(i => i === 0)(indexLens.getOption(state))).toBe(true);
        // expect(exists(i => i === 0)(indexLens.getOption(_))).toBe(false);
        expect(exists(i => i === 0)(none)).toBe(false);

        expect(exists(i => i === 1)(indexLens.getOption(nextItem(state)))).toBe(true);

        // https://gcanti.github.io/fp-ts/modules/Option.ts.html#getorelse

        // screenLens.compose(titleScreen, itemLens)
        //expect().toEqual()
    });
});

// We use a tagged union where there is an unconditional
// "tag" field (enum) and, depending on its value other
// fields.
// In this case we will have several unconditional fields,
// including: progress, tag, and possibily game (if we want)
// to allow for the possibility of resuming a game after
// exiting to the main menu.
// This doesn't make a lot of sense though. Better to have
// a list of saves, so that there can be multiple.
// Then an auto save, which would behave like an unconditional
// game state. But this has the advantage of generalizing.

describe('try index', () => {
    test('get first element of array', () => {
        const xs = [1, 2, 3];
        const firstChild = indexReadonlyArray<number>().index(0);
        // firstChild.getOption(xs) // Optional<number>
        expect(exists(i => i === 1)(firstChild.getOption(xs))).toBe(true);
    })
})