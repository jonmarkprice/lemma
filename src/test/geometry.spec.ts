import { angleFrom, midpoint } from "../core/helpers/geometry";

describe('test angle', () => {
    test('fourth quadrant', () => {
        const p1 = {x: 10, y: -2};
        const p2 = {x: 15, y: -7};
        // (0, 0) to (5, -5)
        expect(angleFrom(p1, p2)).toEqual(-Math.PI/4)
    });

    test('first quadrant', () => {
        const p1 = {x:1, y: 2};
        const p2 = {x:3, y: 8};
        // (0, 0) to (2, 6)
        expect(angleFrom(p1, p2)).toEqual(Math.atan(3))
    });

    test('second quadrant', () => {
        const p1 = {x: -2, y: 0};
        const p2 = {x: -4, y: -2};
        // (0,0) to (-2, -2)
        expect(angleFrom(p1, p2)).toEqual(-Math.PI + Math.PI/4);
    });

    test('third quadrant', () => {
        const p1 = {x: -1, y: 1};
        const p2 = {x: -2 , y: 3};
        // (0, 0) to (-1, 2)
        expect(angleFrom(p1, p2)).toEqual(Math.PI - Math.atan(2));
    });
});

describe('test midpoint', () => {
    test('midpoint between two points', () => {
        expect(midpoint({x: 400, y: 400}, {x: 200, y: 600}))
            .toEqual({x: 300, y: 500});
    })
})