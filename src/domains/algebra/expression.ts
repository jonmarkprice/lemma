import { Operator } from './operators';
import { isEqual, zip } from 'lodash';
import { Copiable, Empty } from '../../utils/copiable';
import { AST, Branch, Leaf } from '../../core/types/ast';

// export interface NodeData {
//   // selected?
// }

export type Expression = AST<Empty, Operator, Term>;
export type Term = Value | Product;

export function showTerm(term: Term): string {
    if (term instanceof Value) {
        return String(term.value)
    } else {
        // Product
        const variables = term.variables.join('');
        if (term.coefficient === 1) {
          return variables;
        } else if (term.coefficient === -1) {
          return '-' + variables;
        } else {
          return String(term.coefficient) + variables;
        }
    }
}

// most of this is generic, but requires a way to compare arbitrary types for the AST
// parameters (A, K, T). We can do this by adding an interface like Copiable
// or we can try to do something like this:
// https://medium.com/@dmkolesnikov/ad-hoc-polymorphism-in-typescript-with-implicit-context-5c11dd668dd

// Alternatively we could pass function (A, A) => boolean, (K, K) => boolean, and (T, T) => boolean
// to compare individual aspects of the general tree
// Then Expression would just need to "implement" three such functions.
export function treeEqual<A extends Copiable<A>, K extends Copiable<K>, T extends Copiable<T>>(
        a: AST<A, K, T>,
        b: AST<A, K, T>,
        compareAnnotation: (a: A, b: A) => boolean, 
        compareKind: (a: K, b: K) => boolean,
        compareValue: (a: T, b: T) => boolean
    ): boolean {
    if (a instanceof Branch && b instanceof Branch) {
        if (a.arity === b.arity) {
            if (compareKind(a.kind, b.kind)) { 
                return zip(a.children, b.children).every(function([a_, b_]) {
                    if (a_ === undefined || b_ === undefined) {
                        return false;
                    } else {
                        return treeEqual(a_, b_, compareAnnotation, compareKind, compareValue);
                    }
                });
            }
        }
    } else if (a instanceof Leaf && b instanceof Leaf) {
        // compare Leafs
        return compareAnnotation(a.annotation, b.annotation) && compareValue(a.value, b.value);
    }
    return false;
}


// TODO: Rewrite in terms of a generic AST comparator and equality methods for
// Product, Value, etc.
export function deepEqual(a: Expression, b: Expression): boolean {
    const compareAnnotation = isEqual;
    const compareKind = (x: Operator, y: Operator) => (x.operator === y.operator);
    const compareValue = (x: Term, y: Term) => {
        if (x instanceof Value && y instanceof Value) {
            return x.value === y.value;
        } else if (x instanceof Product && y instanceof Product) {
            if (x.coefficient === y.coefficient) {
                return isEqual(x.variables, y.variables);
            }
        }
        return false;
    };
    return treeEqual(a, b, compareAnnotation, compareKind, compareValue);
}

export class Product implements Copiable<Product> {
    // N.B. This is extremely similar to TermToken in tokens.ts

    readonly coefficient: number;
    readonly variables: string[];

    // Require at least one variable
    constructor(coefficient: number, ...variables: string[]) {
        this.coefficient = coefficient;

        // Sort at creation time
        if (variables.length === 0) {
            throw new Error('Must include at least one variable');
        }
        variables.sort();

        this.variables = variables;
    }

    copy() {
        return new Product(this.coefficient, ...this.variables)
    }
}

export class Value implements Copiable<Value> {
    readonly value: number;

    constructor(value: number) {
        this.value = value;
    }

    copy() {
        return new Value(this.value);
    } 
}

export function variable(name: string) {
    return new Leaf(new Product(1, name), new Empty());
}

export function value(n: number): AST<Empty, Operator, Term> {
    return new Leaf<Empty, Term>(new Value(n), new Empty());
}

export function product(coefficient: number, ...vars: string[]) {
    return new Leaf(new Product(coefficient, ...vars), new Empty());
}

// TODO: move this
// Example:
// 3x + 7 - 2(x - 1)
// new Minus(
//     new Plus(
//         new Term(3, 'x'), // provide shortcut?
//         new Value(7)),
//     new Mult(
//         new Value(2),
//         new Minus(
//             new Variable('x'),
//             new Value(1)
//         ),
//         {display: false}
//     )
// )