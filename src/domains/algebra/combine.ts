// This should be exactly like the rules, but won't take "templates"
import { Expression, Value, Product, value, product } from './expression';
import { ARITHMETIC_OPERATOR } from './operators';
import { isEqual } from 'lodash';
import { Branch, Leaf } from '../../core/types/ast';

export function combineAtRoot(expr: Expression): Expression {
  // This will perform one of several rules
  if (expr instanceof Branch) {
    if (expr.children[0] instanceof Leaf && expr.children[1] instanceof Leaf) {
      const left = expr.children[0].value;
      const right = expr.children[1].value;
      if (expr.kind.operator === ARITHMETIC_OPERATOR.Addition) {
        // 4 cases
        if (left instanceof Value && right instanceof Value) {
          return value(left.value + right.value);
        }
        else if (left instanceof Product && right instanceof Product) {
          if (isEqual(left.variables, right.variables)) {
            return product(
              left.coefficient + right.coefficient,
              ...left.variables
            );
          }
        }
        // Can't add product and value
      } else { // Multiplication
        // 4 cases
        if (left instanceof Value && right instanceof Value) {
          return value(left.value * right.value);
        }
        else if (left instanceof Value && right instanceof Product) {
          return product(
            left.value * right.coefficient,
            ...right.variables
          );
        } else if (left instanceof Product && right instanceof Value) {
          return product(
            left.coefficient * right.value,
            ...left.variables
          );
        } else if (left instanceof Product && right instanceof Product) {
          // all variables must be unique, otherwise we'd have to square
          // compute union
          const leftVars = new Set(left.variables);
          const rightVars = new Set(right.variables);
          const union = new Set([...leftVars, ...rightVars]);
          if (union.size < leftVars.size + rightVars.size) {
            // there's overlap, so we'd get exponents
            throw new Error('Exponents not implemented');
          } else {
            return product(
              left.coefficient * right.coefficient,
              ...union.values()
            );
          }      
        }
      }
    }
  }
  throw new Error('Cannot combine');
}

// TODO: test
