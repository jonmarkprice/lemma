import { ARITHMETIC_OPERATOR, Operator, showOperator } from './operators';
import { AST, Branch, Leaf } from '../../core/types/ast';
import { Value, Expression, Term } from './expression';
import { Path, PathRelation } from '../../core/types/path';
import { DisplayData } from '../../core/types/displaytree';
import { setFocus } from '../../core/display/focus';
import { Copiable, Empty } from '../../utils/copiable';
import { HIGHLIGHT_COLOR, TEXT_COLOR, GREYED_OUT } from '../../frontend/colors';

// GONNA HAVE TO REWRITE THIS
function mapAnnotation<A extends Copiable<A>, B extends Copiable<B>, K extends Copiable<K>, T extends Copiable<T>>(
    fn: (x: A) => B,
    tree: AST<A, K, T>
): AST<B, K, T> {
    if (tree instanceof Branch) {
        return new Branch(
            tree.kind.copy(),
            fn(tree.annotation),
            ...tree.children.map((child) => mapAnnotation(fn, child))
        );
    } else {
        return new Leaf(tree.value.copy(), fn(tree.annotation));
    }
}

export function renderLaTeX(expr: Expression, focus: Path): string {
    const initSelection = (_: Empty) => new DisplayData(PathRelation.Divergent);
    const tree = mapAnnotation(initSelection, expr);
    const selectionExpr = setFocus(tree, focus);
    return '\\color{' + GREYED_OUT + '}' + renderTree(selectionExpr);
}

function renderTree(tree: AST<DisplayData, Operator, Term>): string {
      // for each of the supported types:
      if (tree instanceof Branch) {
        if (tree.children.length === 2) {
            const str ='(' + renderTree(tree.children[0])
                + ' ' + renderOperator(tree.kind.operator) + ' ' 
                + renderTree(tree.children[1]) + ')';
            if (tree.annotation.selected === PathRelation.Equal) {
                const colored ='\\textcolor{' + TEXT_COLOR +'}{(' + renderTree(tree.children[0])
                + ' \\textcolor{' + HIGHLIGHT_COLOR + '}{' + renderOperator(tree.kind.operator) + '} ' 
                + renderTree(tree.children[1]) + ')}';
                return colored;
            } else {
                return str;
            }
        } else {
            return '[UNSUPPORTED ARITY]';
        }
    } else { // Leaf
        const str = renderTerm(tree.value);
        if (tree.annotation.selected === PathRelation.Equal) {
            const colored = '\\textcolor{' + TEXT_COLOR + '}{' + str + '}';
            return colored;
        } else {
            return str;
        }
    }
}

function renderOperator(op: ARITHMETIC_OPERATOR): string {
    if (op === ARITHMETIC_OPERATOR.Addition) {
        return '+';
    } else if (op === ARITHMETIC_OPERATOR.Multiplication) {
        return '\\cdot';
    }
    throw new Error("Unsupported operator");
}


function renderTerm(term: Term): string {
    if (term instanceof Value) {
        return String(term.value)
    } else {
        // Product
        const variables = term.variables.join('');
        if (term.coefficient === 1) {
            return variables;
        } else if (term.coefficient === -1) {
            return '-' + variables;
        } else {
            return String(term.coefficient) + variables;
        }
    }
}
