import { parse } from '../parser/parser';
import { Value, value } from '../expression';
import { multiply, plus } from '../operators';

describe('Parser', () => {
  test('success cases', () => {
    
    // A literal
    expect(parse('0')).toEqual(value(0));
    
    // A simple addition
    expect(parse('{+ 1 2}'))
      .toEqual(plus(value(1), value(2))); //['+', 1, 2]);
    
    // A nested structure
    expect(parse('{* {+ 9 7} 3}'))
      .toEqual(multiply(
        plus(value(9), value(7)),
        value(3))
      );
  
    // A very deeply right-nested structure
    expect(parse('{+ 1 {+ 2 {+ 3 {+ 4 5}}}}'))
      .toEqual(
        plus(
          value(1),
          plus(
            value(2),
            plus(
              value(3),
              plus(
                value(4),
                value(5)
              )
            )
          )
        )
      );
  });
});

// NOTE:
// There are a whole set of simplification/rewrite
// rules which we might apply implicitly.
// These include:
// - collapsing products to terms
// - dealing with minus signs vs.negative numbers
// - identities 0*x = 0, 1*x = x

// We should have a variety of functions which map
// between equivalent expressions, and check which can
// be simplified.