import { parse } from '../parser/parser';
import { createTreeAndTokens } from '../../../core/evaluation/tree-node';

describe('createTokens', () => {
  test('single token', () => {
    const [, tokens] = createTreeAndTokens(parse('3'));
    expect(tokens).toEqual([{ value: '3', isTerm: true, index: 0 }]);
  });

  test('simple expression', () => {
    const [, tokens] = createTreeAndTokens(parse('{+ 1 2}'))
    expect(tokens).toEqual([
      { value: '(', isTerm: false, index: null },
      { value: '1', isTerm: true, index: 1 },
      { value: '+', isTerm: false, index: 0 },
      { value: '2', isTerm: true, index: 2 },
      { value: ')', isTerm: false, index: null },
    ]);
  });

  test('nested expression', () => {
    const [, tokens] = createTreeAndTokens(parse('{* 8 {+ 3 4}}'));
    expect(tokens).toEqual([
      { value: '(', isTerm: false, index: null },
      { value: '8', isTerm: true, index: 1 },
      { value: '*', isTerm: false, index: 0 },
      { value: '(', isTerm: false, index: null },
      { value: '3', isTerm: true, index: 3 },
      { value: '+', isTerm: false, index: 2 },
      { value: '4', isTerm: true, index: 4 },
      { value: ')', isTerm: false, index: null },
      { value: ')', isTerm: false, index: null },
    ]);
  });
});