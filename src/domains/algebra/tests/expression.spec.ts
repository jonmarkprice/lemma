import { showTerm, Product, variable, Value } from '../expression';

describe("show term", () => {
  test("order of term variables doesn't matter", () => {
    expect(showTerm(new Product(3, 'y', 'x')))
      .toBe('3xy')
    
    expect(new Product(3, 'x', 'y')).toEqual(new Product(3, 'y', 'x'));
  });

  test('all terms shown correctly', () => {
    expect(showTerm(new Product(1, 'x'))).toBe('x');
    expect(showTerm(new Value(-1))).toBe('-1');
    expect(showTerm(new Product(1, 'y'))).toBe('y');
    expect(showTerm(new Product(-1, 'x'))).toBe('-x'); // looks weird
  });
});

// TODO: enfore that variables must be alphabetical