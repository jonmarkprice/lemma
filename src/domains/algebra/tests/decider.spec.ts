import { parses } from '../parser/decider';

// /* GRAMMAR
// exp   ::= '{' id ' ' exp ' ' exp '}' | digit
// id    ::= '+' | '*'
// digit ::= '0'-'9' 
// */
describe('Decider', () => {
    test('literals', () => {
        expect(parses('0')).toBe(true);
        expect(parses('7')).toBe(true);
        expect(parses('10')).toBe(false); // multi-digit not supported
    });

    test('simple expressions', () => {
      expect(parses("{+ 1 2}")).toBe(true);
      expect(parses("{+ {* 1 2} 3}")).toBe(true);
      expect(parses("{* {+ 9 0} {+ 1 9}}")).toBe(true);
    });

    test('failure cases', () => {
        expect(parses("{+}")).toBe(false);
        expect(parses("{+ 1}")).toBe(false);
        expect(parses("+ 1 2")).toBe(false);
        expect(parses("{x 1 2}")).toBe(false);
        expect(parses("{+ 1 2 3}")).toBe(false);
        expect(parses("{* {+ 9 0} {+ 1 9}")).toBe(false);
    });
});