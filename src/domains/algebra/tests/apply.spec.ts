import { applyDirection, applyToRoot, RuleDirection } from '../../../core/evaluation/apply';
import { Rule, terminal } from '../../../core/evaluation/rule';
import { parse } from '../parser/parser';
import { print } from '../parser/helpers';
import { multiply, plus } from '../operators';
import { value, Term, variable } from '../expression';
import * as rules from '../rules';
import { applyAtPath } from '../../../core/evaluation/path';
import { Path } from '../../../core/types/path';

describe('no rule', () => {
  test('sanity test of parse, etc.', () => {
    const input = parse('{+ 2 4}');
    expect(input).toEqual(plus(value(2), value(4)));
  });
});

describe('simple commute', () => {
  test('commute', () => {
    const input = parse('{+ 1 2}');
    expect(applyToRoot(input, rules.COMMUTATIVITY_OF_ADDITION.rule))
      .toEqual(plus(value(2), value(1)));
  });

  test('a more deeply nested one', () => {
    const input = parse('{* {* 1 2} {+ 3 {+ {+ 4 5} 6}}}');
    const result = applyToRoot(input, rules.COMMUTATIVITY_OF_MULTIPLICATION.rule);
    expect(result)
      .toEqual(multiply(
        plus(
          value(3),
          plus<Term>(
            plus<Term>(
              value(4),
              value(5)
            ),
            value(6)
          ),
        ),
        multiply(value(1), value(2))
      ));
    
    // This is a lot easier to write / read
    expect(print(result))
      .toBe('((3 + ((4 + 5) + 6)) * (1 * 2))');
  });
});

describe('generalized apply', () => {
  test('identity rule', () => {
    const identity = new Rule(terminal(1), terminal(1));
    const given = plus<Term>(variable('x'), value(1));
    expect(applyToRoot(given, identity)).toEqual(given);
  });

  test('commutative property of addition)', () => {
    const given = plus<Term>(value(3), value(1));
    const result = plus<Term>(value(1), value(3));
    expect(applyToRoot(given, rules.COMMUTATIVITY_OF_ADDITION.rule))
      .toEqual(result);
  })

  test('associative property of addition', () => {
    const given = parse('{+ 4 {+ 3 7}}');
    const result = parse('{+ {+ 4 3} 7}');
    expect(applyToRoot(given, rules.ASSOCIATIVITY_OF_ADDITION.rule))
      .toEqual(result);
  });

  test('associative property (reversed)', () => {
    const given = parse('{+ {+ 4 3} 7}');
    const result = parse('{+ 4 {+ 3 7}}');
    expect(applyToRoot(given, rules.ASSOCIATIVITY_OF_ADDITION.rule))
      .toEqual(result);
  });

  test('distributive property', () => {
    const given = parse('{* 8 {+ 2 3}}');
    const result = parse('{+ {* 8 2} {* 8 3}}');
    expect(applyToRoot(given, rules.DISTRIBUTIVE_PROPERTY.rule))
      .toEqual(result);
  })

  test('factoring', () => {
    const result = parse('{+ {* 8 2} {* 8 3}}');
    const given = parse('{* 8 {+ 2 3}}');
    expect(applyToRoot(given, rules.DISTRIBUTIVE_PROPERTY.rule))
      .toEqual(result);
  });

  test('bad factor throws', () => {
    const given = parse('{+ {* 8 2} {* 7 3}}');
    try {
      expect(applyToRoot(given, rules.DISTRIBUTIVE_PROPERTY.rule))
        .toThrow();
    } catch (e) {
      expect(e).toEqual(new Error('Does not match previously saved value'));
    }
  });
});

describe('rules applied to subexpressions', () => {
  test('additive commutativity', () => {
    const given = parse('{+ 2 {* 4 {+ 9 3}}}');
    expect(print(applyAtPath(given, [2, 2], rules.ADD_COM.rule, RuleDirection.RTL)))
      .toEqual('(2 + (4 * (3 + 9)))');
  });
});

describe('rules with constants', () => {
  test('zero elimination', () => {
    const given = parse('{+ 1 0}');
    expect(applyAtPath(given, [], rules.ADDITIVE_IDENTITY.rule, RuleDirection.RTL))
      .toEqual(value(1));
  });

  test('zero introduction', () => {
    const given = variable('x');
    expect(applyAtPath(given, [], rules.ADDITIVE_IDENTITY.rule, RuleDirection.LTR))
      .toEqual(plus<Term>(variable('x'), value(0)));
  });

  test('nested zero introduction', () => {
    const given = parse('{* {+ 1 3} {+ 5 3}}');
    const rule = rules.ADDITIVE_IDENTITY.rule;
    const path: Path = [2, 1];
    const result = applyAtPath(given, path, rule, RuleDirection.LTR);
    // expect(result).toEqual(parse('{* {+ 1 3} {+ {+ 5 0} 3}}'));
    expect(print(result)).toEqual('((1 + 3) * ((5 + 0) + 3))');
  });

  test('simple directional zero introduction', () => {
    const given = parse('3');
    const rule = rules.ADDITIVE_IDENTITY.rule;
    const result = applyDirection(given, rule, RuleDirection.LTR);
    expect(print(result)).toEqual('(3 + 0)');
  });

  test('another directional zero introduction', () => {
    const given = parse('{* 5 7}');
    const rule = rules.ADDITIVE_IDENTITY.rule;
    const result = applyDirection(given, rule, RuleDirection.LTR);
    expect(print(result)).toEqual('((5 * 7) + 0)');
  });

  test('unit introduction (multplication)', () => {
    const given = variable('x');
    expect(applyToRoot(given, rules.MULTIPLICATIVE_IDENTITY.rule))
      .toEqual(multiply<Term>(value(1), variable('x')));
  });

  test('coefficient elimination', () => {
    const given = multiply<Term>(value(1), variable('x'));
    expect(applyDirection(given, rules.MULTIPLICATIVE_IDENTITY.rule, RuleDirection.RTL))
      .toEqual(variable('x'));
  });
})

// TODO: write unit test showing that factoring is not strict enough
// since we don't compare nodes!!!

// maybe write a function: invert :: Rule -> Rule
// which flips left and right.
// validate that their composition is equivalent to idenitity.