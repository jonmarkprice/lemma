import { combineAtRoot } from '../combine';
import { value, product, Term, variable } from '../expression';
import { plus, multiply } from '../operators'

describe('basic arithmetic', () => {
  test('addition', () => {
    expect(combineAtRoot(plus(value(1), value(2))))
      .toEqual(value(3));
  });

  test('multiplication', () => {
    expect(combineAtRoot(multiply(value(2), value(7))))
      .toEqual(value(14));
  });
});

describe('like terms', () => {
  test('add like terms', () => {
    expect(combineAtRoot(plus<Term>(product(3, 'x'), product(4, 'x'))))
      .toEqual(product(7, 'x'));
  });

  test('add variables', () => {
    expect(combineAtRoot(plus<Term>(variable('x'), variable('x'))))
      .toEqual(product(2, 'x'));
  });

  test('2 * x = 2x', () => {
    expect(combineAtRoot(multiply<Term>(
      variable('x'), value(2)
    ))).toEqual(
      product(2, 'x')
    );
  });
  test('x * 2 = 2x', () => {
    // order shouldn't matter
    expect(combineAtRoot(multiply<Term>(
      variable('x'), value(2),
    ))).toEqual(
      product(2, 'x')
    );
  });
  test('3x * 2 = 6x', () => {
    expect(combineAtRoot(multiply<Term>(
      product(3, 'x'), value(2)
    ))).toEqual(
      product(6, 'x')
    );
  });
  test('x * y = xy', () => {
    expect(combineAtRoot(multiply<Term>(
      variable('x'), variable('y')
    ))).toEqual(
      product(1, 'x', 'y')
    )
  });
  test('y * x = xy', () => {
    expect(combineAtRoot(multiply<Term>(
      variable('y'), variable('x')
    ))).toEqual(
      product(1, 'x', 'y')
    )
  });
  test('2x * 3y = 6xy', () => {
    expect(combineAtRoot(multiply<Term>(
      product(2, 'x'),
      product(3, 'y')
    ))).toEqual(
      product(6, 'x', 'y')
    )
  });
  test('5xz * 3yw = 15wxyz', () => {
    expect(combineAtRoot(multiply<Term>(
      product(5, 'x', 'z'),
      product(3, 'y', 'w')
    ))).toEqual(
      product(15, 'w', 'x', 'y', 'z')
    );
  })
  test('3x * 4xy throws', () => {
    try {
      expect(combineAtRoot(multiply<Term>(
        product(3, 'x'),
        product(4, 'x', 'y')
      ))).toThrow();
    } catch (e) {
      expect(e).toEqual(new Error('Exponents not implemented'));
    }
  })
});

// edge case 0x -> 0

