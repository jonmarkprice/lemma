import Input from '../../../core/parser/input';
import { value, Expression } from '../expression';
import { multiply, plus } from '../operators';

// Cannot (easily) express recursive structure in TS
// type Tree = number | [Op, Tree, Tree] | Error;

// possibly rename to expect
function literal(input: Input, ...chars: string[]): string {
  for (let char of chars) {
    if (input.current() === char) {
      input.read();
      return char;
    }
  }
  throw new Error(`Expected one of ${chars.join(", ")}, got ${input.current()}`);
}

/* EBNF GRAMMAR
exp   ::= '{' id ' ' exp ' ' exp '}' | digit
id    ::= '+' | '*'
digit ::= '0'-'9' 
*/
export function parse(s: string): Expression {
  const input = new Input(s);
  
  // Empty input case
  if (input.done()) {
    throw new Error("No input");
  }

  try {
    const tree: Expression = exp(input);
    if (input.done() && !input.overflowed()) {
      return tree;
    } else {
      throw new Error('Input error');
    }
  } catch(err) {
    throw err; // What's the point of this?
  }
}

// TODO: Let's try to type as Expr

// exp ::= '{' id ' ' exp ' ' exp '}' | digit
function exp(input: Input): Expression {
  if (input.current() === '{') {
    literal(input, '{');
    const fn = id(input);
    literal(input, ' ');
    const left = exp(input);
    literal(input, ' ');
    const right = exp(input);
    literal(input, '}');

    // return [fn, left, right];
    switch (fn) {
      case '*': return multiply(left, right);
      case '+': return plus(left, right);
      default: throw new Error(`Unexpected id: ${fn}.`);
    }
  } else {
    return digit(input);
  }
}

// id ::= '+' | '*'
function id(input: Input): string {
  return literal(input, '+', '*');
}

// digit ::= '0'-'9'
function digit(input: Input): Expression {
  if (/^[0-9]$/.test(input.current())) {
    const tok = input.current();
    input.read();
    return value(Number(tok));
  } else {
    throw new Error(`Expected digit, got ${input.current()}`);
  }
}