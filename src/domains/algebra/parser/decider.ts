import Input from '../../../core/parser/input'

function literal(input: Input, ...chars: string[]) {
  for (let char of chars) {
    if (input.current() === char) {
      input.read();
      return;
    }
  }
  throw new Error(`Expected one of ${chars.join(", ")}, got ${input.current()}`);
}

/* GRAMMAR
exp   ::= '{' id ' ' exp ' ' exp '}' | digit
id    ::= '+' | '*'
digit ::= '0'-'9' 
*/
export function parses(s: string): boolean {
  const input = new Input(s);
  
  // Empty input case
  if (input.done()) {
    return true;
  }

  try {
    exp(input);
  } catch(err) {
    // console.error(err);
    return false;
  }

  return input.done() && !input.overflowed();
}

// exp ::= '{' id ' ' exp ' ' exp '}' | digit
function exp(input: Input) {
  if (input.current() === '{') {
    literal(input, '{');
    id(input);
    literal(input, ' ');
    exp(input);
    literal(input, ' ');
    exp(input);
    literal(input, '}');
  } else {
    digit(input);
  }
}

// id ::= '+' | '*'
function id(input: Input) {
  literal(input, '+', '*');
}

// digit ::= '0'-'9'
function digit(input: Input) {
  if (/^[0-9]$/.test(input.current())) {
    input.read();
  } else {
    throw new Error(`Expected digit, got ${input.current()}`);
  }
}
