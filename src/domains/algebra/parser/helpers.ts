import { Value, Expression } from '../expression';
import { Operator, showOperator } from '../operators';
import { Branch } from '../../../core/types/ast';
import { showTerm } from '../expression';

export function print(tree: Expression): string {
  // for each of the supported types:
    if (tree instanceof Branch) {
        if (tree.children.length === 2) {
            return `(${print(tree.children[0])} ${showOperator(tree.kind.operator)} ${print(tree.children[1])})`;
        } else {
            return '[UNSUPPORTED ARITY]';
        }
    } else { // Leaf
        return showTerm(tree.value);
    }
}
