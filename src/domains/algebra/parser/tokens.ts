export enum LiteralToken {
  OpenParen, // = '(',
  CloseParen, // = ')',
  PlusSign, // = '+',
  MinusSign, // = '-',
  MultiplicationSign, // = '*',
};

export type Token = LiteralToken | TermToken

export class TermToken {
  readonly showCoefficient: boolean = false;
  readonly coefficient: number;
  readonly variables: string[];

  constructor(coefficient: number | null, ...variables: string[]) {
    // Three valid cases
    // 1. (null, [x]) -> variable
    // 2. (x, []) -> constant
    // 3. (_, [x, ...]) -> term (e.g. 3x or xy)
    // One invalid
    // 1. (null, []) -> INVALID
    // N.B. Special case but won't deal with it here
    // (0, _) -> constant (0)

    if (coefficient === null) {
      if (variables.length === 0) {
        throw new Error('Empty term');
      }
      this.coefficient = 1;
      this.showCoefficient = false;
    } else {
      this.coefficient = coefficient;
      this.showCoefficient = true;
    }
    this.variables = variables;
  }
}
