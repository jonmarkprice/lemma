import { Copiable, Empty } from "../../utils/copiable";
import { AST, Branch } from '../../core/types/ast';

// TODO: DEPRECATE
// TODO: Make Expr also support UnaryNodes
// export type Expr<T extends Copiable<T>> = BinaryOperator<T> | T;

// Exported Enum used to check type of operator.
export enum ARITHMETIC_OPERATOR {
    Addition,
    Multiplication,
    Subtraction,
    Negative,
    Exponentiate,
}

// Secret, unexported symbols so no one else can construct an Operator
const ADDITION =  Symbol('addition');
const MULTIPLICATION = Symbol('multiplication');
const SUBTRACTION = Symbol('subtraction');
const NEGATIVE = Symbol('negative');
const EXPONENTIATE = Symbol('exponentiate');

// Maybe we could make it take one of two unexported symbols?
export class Operator implements Copiable<Operator> {
    readonly operator: ARITHMETIC_OPERATOR;
    constructor(
        operator: Symbol,
    ) {
        if (operator === ADDITION) {
            this.operator = ARITHMETIC_OPERATOR.Addition;
        } else if (operator === MULTIPLICATION) {
            this.operator = ARITHMETIC_OPERATOR.Multiplication;
        } else if (operator === SUBTRACTION) {
            this.operator = ARITHMETIC_OPERATOR.Subtraction;
        } else if (operator === NEGATIVE) {
            this.operator = ARITHMETIC_OPERATOR.Negative;
        } else if (operator === EXPONENTIATE) {
            this.operator = ARITHMETIC_OPERATOR.Exponentiate;
        } else {
            throw Error("Don't use this directly!");
        }
    }

    copy(): Operator {
        if (this.operator === ARITHMETIC_OPERATOR.Addition) {
            return new Operator(ADDITION);
        } else if (this.operator === ARITHMETIC_OPERATOR.Multiplication) {
            return new Operator(MULTIPLICATION);
        } else if (this.operator === ARITHMETIC_OPERATOR.Subtraction) {
            return new Operator(SUBTRACTION);
        } else if (this.operator === ARITHMETIC_OPERATOR.Negative) {
            return new Operator(NEGATIVE);
        } else if (this.operator === ARITHMETIC_OPERATOR.Exponentiate) {
            return new Operator(EXPONENTIATE);
        } else {
            throw TypeError('Unknown operator type');
        }
    } 
} 

type OpFunction<V extends Copiable<V>>
    = (left: AST<Empty, Operator, V>, right: AST<Empty, Operator, V>)
    => AST<Empty, Operator, V>;

export function plus<V extends Copiable<V>>(
    left: AST<Empty, Operator, V>, 
    right: AST<Empty, Operator, V>
): AST<Empty, Operator, V> {
    return new Branch(new Operator(ADDITION), new Empty(), left, right);
}

export function multiply<V extends Copiable<V>>(
    left: AST<Empty, Operator, V>,
    right: AST<Empty, Operator, V>,
): AST<Empty, Operator, V> {
    return new Branch(new Operator(MULTIPLICATION), new Empty(), left, right);
}

export function minus<V extends Copiable<V>>(
    left: AST<Empty, Operator, V>,
    right: AST<Empty, Operator, V>,
): AST<Empty, Operator, V> {
    return new Branch(new Operator(SUBTRACTION), new Empty(), left, right);
}

export function negative<V extends Copiable<V>>(
    operand: AST<Empty, Operator, V>,
): AST<Empty, Operator, V> {
    return new Branch(new Operator(NEGATIVE), new Empty(), operand);
}

export function power<V extends Copiable<V>>(
    base: AST<Empty, Operator, V>,
    power: AST<Empty, Operator, V>,
): AST<Empty, Operator, V> {
    return new Branch(new Operator(EXPONENTIATE), new Empty(), base, power);
}

export function createOperator<V extends Copiable<V>>(
    op: ARITHMETIC_OPERATOR
): OpFunction<V> {
    if (op === ARITHMETIC_OPERATOR.Addition) {
        return plus;
    } else if (op === ARITHMETIC_OPERATOR.Multiplication) {
        return multiply;
    } else if (op === ARITHMETIC_OPERATOR.Subtraction) {
        return minus;
    } else if (op === ARITHMETIC_OPERATOR.Negative) {
        return negative;
    } else if (op === ARITHMETIC_OPERATOR.Exponentiate) {
        return power;
    }
    throw new Error("Unsupported operator");
}

export function showOperator(op: ARITHMETIC_OPERATOR): string {
    if (op === ARITHMETIC_OPERATOR.Addition) {
        return '+';
    } else if (op === ARITHMETIC_OPERATOR.Multiplication) {
        return '*';
    } else if (op === ARITHMETIC_OPERATOR.Subtraction) {
        return '-';
    } else if (op === ARITHMETIC_OPERATOR.Exponentiate) {
        return '^';
    }
    throw new Error("Unsupported operator");
}