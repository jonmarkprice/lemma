// For algebra
import { title } from 'process';
import { Direction } from '../../core/display/focus';
import { RuleDirection } from '../../core/evaluation/apply';
import { Rule, TemplateNode, terminal, constant } from '../../core/evaluation/rule';
import { multiply, plus, minus, negative, power } from './operators';

// TODO: move into core
// Potentially replace two directions a list of forms under
// the same name.
export interface RuleObj {
    readonly rule: Rule,
    readonly title: string,
    readonly value: string, // deprecate
    readonly direction: RuleDirection, // we could potentially avoid baking this in using a intersection type?
};

export const ID: RuleObj = {
    rule: new Rule(terminal(1), terminal(1)),
    title: 'identity',
    value: 'a = a',
    direction: RuleDirection.LTR,
};

// --- addition ---
// ADD_ASSOC
// flipped, may need to change unit tests
export const ADD_ASSOC: RuleObj = {
    rule: new Rule(
        plus(
            plus(
                terminal(1),
                terminal(2),
            ),
            terminal(3)
        ),
        plus(
            terminal(1),
            plus(
                terminal(2),
                terminal(3)
            )
        )
    ),
    title: 'Associative (+)',
    value: '(a + b) + c = a + (b + c)',
    direction: RuleDirection.LTR,
};
export const ASSOCIATIVITY_OF_ADDITION = ADD_ASSOC; // TODO: sides

// ADD_ID [INTRO/ELIM]
export const ADD_ID: RuleObj = {
    rule: new Rule(
        terminal(1),
        plus<TemplateNode>(
            terminal(1),
            constant(0)
        )
    ),
    title: 'Add zero',
    value: 'a = a + 0',
    direction: RuleDirection.LTR,
};
export const ADDITIVE_IDENTITY = ADD_ID;

// ADD_INV
export const ADD_INV: RuleObj = {
    rule: new Rule(
        plus(terminal(1), negative(terminal(1))),
        constant(0)
    ),
    title: 'Additive Inverse',
    value: 'a + (-a) = 0',
    direction: RuleDirection.LTR,
}

// ADD_COM
export const ADD_COM: RuleObj = {
    rule: new Rule(
        plus(terminal(1), terminal(2)),
        plus(terminal(2), terminal(1))
    ),
    title: 'Commutative (+)',
    value: 'a + b = b + a',
    direction: RuleDirection.LTR,
};
export const COMMUTATIVITY_OF_ADDITION = ADD_COM; // alias

// --- multiplication ---
// MULT_ASSOC
export const MULT_ASSOC: RuleObj = {
    rule: new Rule(
        multiply(multiply(terminal(1), terminal(2)), terminal(3)),
        multiply(terminal(1), multiply(terminal(2), terminal(3)))
    ),
    title: 'Multiplicative Associativity',
    value: '(a * b) * c = a * (b * c)',
    direction: RuleDirection.LTR,
}

// MULT_ID
export const MULT_ID: RuleObj = {
    rule: new Rule(
        terminal(1),
        multiply<TemplateNode>(
            constant(1),
            terminal(1)
        )
    ),
    title: 'Identity (*)',
    value: 'a = 1 * a',
    direction: RuleDirection.LTR,
};
export const MULTIPLICATIVE_IDENTITY = MULT_ID; // alias

// MULT_INV
// TODO: must introduce frac first.

// MULT_COMM
export const MULT_COM: RuleObj = {
    rule: new Rule(
        multiply(terminal(1), terminal(2)),
        multiply(terminal(2), terminal(1))
    ),
    title: 'Commutative (*)',
    value: 'a * b = b * a',
    direction: RuleDirection.LTR,
};
export const COMMUTATIVITY_OF_MULTIPLICATION = MULT_COM;

// --- distributive ---

export const DISTRIBUTIVE_PROPERTY: RuleObj = {
    rule: new Rule(
        multiply(
            terminal(1),
            plus(
                terminal(2),
                terminal(3)
            )
        ),
        plus(
            multiply(
                terminal(1),
                terminal(2)
            ),
            multiply(
                terminal(1),
                terminal(3)
            )
        )
    ),
    title: 'Distributive Property',
    value: 'a(b + c) = ab + ac',
    direction: RuleDirection.LTR,
};

// --- notational ---

export const SUBTRACTION_PROPERTY: RuleObj = {
    rule: new Rule(
        minus(terminal(1), terminal(2)),
        plus(terminal(1), negative(terminal(2))),
    ),
    title: 'Subtraction Property',
    value: 'a - b = a + (-b)',
    direction: RuleDirection.LTR,
}

export const NEGATIVE_PROPERTY: RuleObj = {
    rule: new Rule(
        negative(terminal(1)),
        multiply<TemplateNode>(constant(-1), terminal(1))
    ),
    title: 'Negative Property',
    value: '-a = -1 * a',
    direction: RuleDirection.LTR,
}

export const EXPONENTIAL_IDENTITY: RuleObj = {
    rule: new Rule(
        power<TemplateNode>(terminal(1), constant(1)),
        terminal(1),
    ),
    title: 'Exponential idenity',
    value: 'a^1 = a',
    direction: RuleDirection.LTR,
}

const MULTIPLYING_POWERS: RuleObj = {
    rule: new Rule(
        multiply(power(terminal(1), terminal(2)), power(terminal(1), terminal(3))),
        power(terminal(1), plus(terminal(2), terminal(3)))
    ),
    title: 'Multiplying powers',
    value: 'a^b * a^c = a^{b + c}',
    direction: RuleDirection.LTR,
}

// --- derived ---

export const ZERO_ABSORBTION: RuleObj = {
    rule: new Rule(
        multiply<TemplateNode>(terminal(1), constant(0)),
        constant(0)
    ),
    title: 'Zero cancels',
    value: 'a * 0 = 0',
    direction: RuleDirection.LTR,
}

const allRules = [
  ASSOCIATIVITY_OF_ADDITION,
  ADDITIVE_IDENTITY,
  // additiveInverse,
  COMMUTATIVITY_OF_ADDITION,
  // multiplicativeAssociativity,
  MULTIPLICATIVE_IDENTITY,
  COMMUTATIVITY_OF_MULTIPLICATION,
  DISTRIBUTIVE_PROPERTY,
  NEGATIVE_PROPERTY,
  SUBTRACTION_PROPERTY,
  MULTIPLYING_POWERS,
  ZERO_ABSORBTION,
]

export default allRules;