export interface Copiable<T> {
    copy(): T;
}

export class Empty implements Copiable<Empty> {

    copy() {
        return new Empty();
    }
}

export class Primitive<T> {
    readonly value: T;

    constructor(value: T) {
        this.value = value;
    }

    copy(): Primitive<T> {
        return new Primitive(this.value)
    }
}